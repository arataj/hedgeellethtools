/*
 * PAdversary.java
 *
 * Created on Mar 26, 2013
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.tools;

import java.util.*;
import java.io.*;

import org.jblas.DoubleMatrix;
import org.jblas.Solve;

import pl.gliwice.iitis.hedgeelleth.compiler.util.CompilerUtils;
import pl.gliwice.iitis.hedgeelleth.math.matrix.solve.MatrixOp;

/**
 * Loads a Prism's adversary, from files <code>*.states</code> and
 * <code>*.strategy</code>.
 * 
 * @author Artur Rataj
 */
public class PAdversary {
    /**
     * A single transition.
     */
    public static class Transition {
        /**
         * Index of the target state.
         */
        public final long targetIndex;
        /**
         * Probability of reaching the target state.
         */
        public final double probability;
        /**
         * Choice index, -1 for none.
         */
        public final int choiceIndex;

        public Transition(long targetIndex, double probability, int choiceIndex) {
            this.targetIndex = targetIndex;
            this.probability = probability;
            this.choiceIndex = choiceIndex;
        }
        @Override
        public String toString() {
            return probability + ":" + targetIndex;
        }
    }
    /**
     * Number of states.
     */
    public long statesNum;
    /**
     * States, over which is defined this adversary.
     */
    public PStates states;
    /**
     * An optional strategy.
     */
    PStrategy strategy;
    /**
     * Map of transitions for subsequent states, keyed with a state index.
     * Size of this map can be smaller than <code>statesNum</code>, as
     * some states might be missing from the adversary. Probabilities in each
     * of the lists should sum to 1.
     */
    public SortedMap<Long, List<Transition>> transitionsMap;
    /**
     * Creates a new adversary.
     * 
     * @param states set of states
     * @param traFilename name of the file with an adversary; alternately,
     * a full transition matrix then filtered by <code>strategy</code>
     * @param strategy if not null, filters the transitions in <code>traFileName</code>;
     * also changes all original choice indexes to these in the strategy
     */
    public PAdversary(PStates states, String traFilename, PStrategy strategy) throws IOException {
        this.states = states;
        this.strategy = strategy;
        transitionsMap = new TreeMap<>();
        InputStream in = new FileInputStream(traFilename);
        Scanner scLine = CompilerUtils.newScanner(in);
        boolean headerLine = true;
        long lineCount = 0;
        long prevSource = -1;
        if(strategy == null)
            System.out.println("loading adversary...");
        else
            System.out.println("loading transitions to be filtered by strategy...");
        while(scLine.hasNextLine()) {
            ++lineCount;
            final int LINE_M = 1000000;
            if(lineCount%(LINE_M + 1) == LINE_M)
                System.out.println(lineCount/LINE_M + "m");
            String line = scLine.nextLine();
            if(line.isEmpty())
                continue;
            if(headerLine) {
                if(line.charAt(line.length() - 1) == '?')
                    line = line.substring(0, line.length() - 1).trim();
                Scanner scNumbers = CompilerUtils.newScanner(line).useDelimiter(" ");
                if(!scNumbers.hasNextLong())
                    throw new IOException(traFilename + ": " + lineCount +
                            ": missing number of states");
                statesNum = scNumbers.nextLong();
                if(statesNum != this.states.getStatesNum())
                    throw new IOException(traFilename + ": " + lineCount +
                            ": number of states in the adversary greater than the total  " +
                            "number of states = " + this.states.getStatesNum());
                headerLine = false;
            } else {
                Scanner scNumbers = CompilerUtils.newScanner(line).useDelimiter(" ");
                if(!scNumbers.hasNextLong())
                    throw new IOException(traFilename + ": " + lineCount +
                            ": missing source state index");
                long source = scNumbers.nextLong();
                if(!scNumbers.hasNextInt())
                    throw new IOException(traFilename + ": " + lineCount +
                            ": missing choice index");
                int choiceIndex = scNumbers.nextInt();
                if(!scNumbers.hasNextLong())
                    throw new IOException(traFilename + ": " + lineCount +
                            ": missing target state index");
                long target = scNumbers.nextLong();
//                if(!scNumbers.hasNextDouble())
//                    throw new IOException(traFilename + ": " + lineCount +
//                            ": missing transition probability");
                // scNumbers.next()
                double probability = scNumbers.nextDouble();
                int choice;
                if(scNumbers.hasNext()) {
                    if(strategy == null)
                        choice = -1;
                    else {
                        String name = scNumbers.next();
                        choice = strategy.NAME.indexOf(name);
                        if(choice == -1)
                            // such a choice is not present in the strategy at all
                            choice = Integer.MAX_VALUE;
                    }
                } else
                    choice = -1;
                boolean enable;
                if(choice == -1) {
                    enable = true;
                    choice = choiceIndex;
                } else {
                    // check if reachable, then if the choice matches
                    enable = strategy.CHOICE.get(source) != null &&
                            strategy.CHOICE.get(source) == choice;
                }
                if(enable) {
                    List<Transition> xList = transitionsMap.get(source);
                    if(xList == null) {
                        xList = new LinkedList<>();
                        transitionsMap.put(source, xList);
                    }
                    xList.add(new Transition(target, probability, choice));
                }
                if(source < prevSource)
                    throw new IOException(traFilename + ": " + lineCount +
                            ": unordered source state");
                prevSource = source;
            }
        }
        final double EPSILON = 1e-6;
        for(long source : transitionsMap.keySet()) {
            List<Transition> xList = transitionsMap.get(source);
            double sum = 0.0;
            for(Transition x : xList)
                sum += Math.abs(x.probability);
            if(Math.abs(sum - 1.0) > EPSILON)
                throw new IOException(traFilename + ": probabilities of transitions " +
                        "from state " + source + " sum to " + sum);
            if(source > Integer.MAX_VALUE)
                // <code>PStates</code> can not address indices outside
                // the range of <code>int</code>
                throw new RuntimeException("index too large");
        }
        System.out.println("loaded transitions for " + transitionsMap.size() + " states, ok.");
    }
    /**
     * Returns the set of states, over which this advesary is defined.
     * 
     * @return a set of states
     */
    public PStates getStates() {
        return states;
    }
    /**
     * Returns a sorted set of indices of source states in
     * this adversary.
     * 
     * @return a set of state indices in an increasing order
     */
    public Set<Long> getSourceStates() {
        return transitionsMap.keySet();
    }
    /**
     * Returns a list of transitions, from a given source state.
     * 
     * @param sourceIndex index of the source state, 0 ...
     * <code>getStates().getStatesNum()</code> - 1
     * @return a list of transitions or null, if a state of a given index
     * does not exist in this adversary
     */
    public List<Transition> getTransitions(long sourceIndex) {
        if(sourceIndex == -1)
            throw new RuntimeException("state index unknown");
        if(sourceIndex >= statesNum)
            throw new RuntimeException("state index too large");
        return transitionsMap.get(sourceIndex);
    }
    /**
     * Probability of eventually reaching a state of a given index.
     * 
     * @param i index of the state
     * @return probability
     */
    public double[] reachability(int i) {
        boolean SPLIT = false;
        int sNum = states.getStatesNum();
        int size = sNum;
        if(SPLIT)
            for(int s = 0; s < sNum; ++s)
                for(Transition t : transitionsMap.get((long)s))
                    if(t.targetIndex == s /*&& t.probability < 0.0*/)
                        // a place for a negation state
                        ++size;
        DoubleMatrix a = new DoubleMatrix(size, size);
        DoubleMatrix b = new DoubleMatrix(size);
        int negationStates = 0;
        double EPSILON = 1e-12;
        for(int s = 0; s < sNum; ++s) {
            a.put(s, s, 1 + EPSILON); // x_s
            if(s != i) {
                List<Transition> out = transitionsMap.get((long)s);
                for(Transition t : out)
                    if(t.targetIndex != s) {
                        a.put(s, (int)t.targetIndex, -t.probability);
                    } else if(true || t.probability < 0.0) {
                        // negates the event
                        if(SPLIT) {
                            int j = sNum + negationStates;
                            a.put(s, j, -t.probability);
                            a.put(j, j, 1);
                            a.put(j, s, -t.probability);
                            for(Transition negT : out)
                                if(negT.targetIndex != s)
                                    a.put(j, (int)negT.targetIndex, -negT.probability);
                            ++negationStates;
                        } else
                            a.put(s, s, 1.0 + EPSILON - t.probability);
                    }
            } else {
                b.put(s, 0, 1.0); // probability of reaching i from i is 1
            }
        }
//        DoubleMatrix s = Solve.solve(a, b);
        DoubleMatrix s = MatrixOp.gaussSeidel(a, b);
        return s.toArray();
    }
    /**
     * Saves the adversary in the MDP tra format. Does not close the stream.
     */
    public void saveTra(PrintWriter out) throws IOException {
        out.println(states.numStates);
        for(long s = 0; s < states.numStates; ++s) {
            List<Transition> o = transitionsMap.get(s);
            if(o != null) {
                for(Transition t : o) {
                    String name;
                    if(strategy == null)
                        name = "";
                    else
                        name = strategy.NAME.get(t.choiceIndex);
                    out.println(s + " " + t.choiceIndex + " " + t.targetIndex +
                            " " + t.probability + " " + name);
                }
            }
        }
    }
    /**
     * Saves the adversary in the MDP tra format.
     * 
     * This is a convenience method.
     */
    public void saveTra(String fileName) throws IOException {
        System.out.println("saving adversary...");
        PrintWriter out = new PrintWriter(fileName);
        saveTra(out);
        out.close();
        // System.out.println("ok.");
    }
    @Override
    public String toString() {
        StringBuilder out = new StringBuilder();
        out.append(states.toString());
        ByteArrayOutputStream traOut = new ByteArrayOutputStream();
        PrintWriter tra = new PrintWriter(traOut);
        try {
            saveTra(tra);
        } catch(IOException e) {
            throw new RuntimeException("unexpected");
        }
        tra.close();
        try {
            out.append(traOut.toString("UTF-8"));
        } catch(UnsupportedEncodingException e) {
            throw new RuntimeException("unexpected");
        }
        return out.toString();
    }
    public static void main(String[] args) throws IOException {
        String[] args_ = {
            "/home/art/projects/svn/HedgeellethCompiler/example/simple/CoinFlip.states",
            "/home/art/projects/svn/HedgeellethCompiler/example/simple/CoinFlip.tra",
            null
        };
        //String prefix = "/home/art/swistak/modelling/abs3/" + "W3_";
        String prefix = "/home/art/projects/svn/HedgeellethOptimiser/" +
                "CoinFlip";
        String[] args__ = {
            prefix + ".sta",
            prefix + ".tra",
            prefix + ".strategy",
            prefix + ".adv",
        };
        // args = args__;
        if(args.length < 2 || args.length > 4)
            System.out.println("syntax: <Prism's states file> " +
                    "<Prism's adversary or transition file> " +
                    "[ <Prism's strategy file to filter the transition file> ]");
        else {
            PStates s = new PStates(args[0], null);
            PStrategy t;
            if(args.length >= 3)
                t = new PStrategy(s, args[2]);
            else
                t = null;
            PAdversary a = new PAdversary(s, args[1], t);
            if(args.length >= 4)
                a.saveTra(args[3]);
        }
    }
}
