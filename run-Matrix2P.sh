#
# syntax: <plain dense matrix file> <output nm file>
# if the last state is absorbing, a respective label \"absorbing\" is generated
#
java -cp dist/HedgeellethTools.jar pl.gliwice.iitis.hedgeelleth.tools.Matrix2P $*
