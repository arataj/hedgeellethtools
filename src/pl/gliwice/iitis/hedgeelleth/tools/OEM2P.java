/*
 * OEM2P.java
 *
 * Created on May 10, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.tools;

import java.util.*;
import java.io.*;

import pl.gliwice.iitis.hedgeelleth.compiler.util.text.NumStream;
import pl.gliwice.iitis.hedgeelleth.math.Xxy;
import pl.gliwice.iitis.hedgeelleth.math.rng.ArrayDist;
import pl.gliwice.iitis.hedgeelleth.math.rng.OlssonEM;

/**
 * Olsson's [1] iterative fitting to a probability density function, using
 * a phase--type distribution.
 * 
 * S. Asmussen, O. Nerman & M. Olsson, Fitting phase-type distribution via the EM
 * algorithm, Scand. J. Statist. 23, 419-441 (1996).
 * 
 * @author Artur Rataj
 */
public class OEM2P {
    /*
     * Generates a list of comments for a Cox distribution.
     * 
     * @param matrix representing the distribution
     * @return list or row comments
     */
    protected static List<String> getRowComments(double[][] matrix) {
        List<String> out = new LinkedList<>();
        int phases = matrix.length - 1;
        for(int row = 0; row < phases; ++row) {
            double sink = matrix[row][phases];
            int index = row + 1;
            double mu;
            double alpha;
            if(row != phases - 1) {
                double next = matrix[row][row + 1];
                mu = next + sink;
                alpha = next/(next + sink);
            } else {
                mu = sink;
                alpha = Double.NaN;
            }
            String s = "mu_" + index + " = " + mu +
                    (!Double.isNaN(alpha) ? ", alpha_" + index + " = " +
                    alpha : "");
            out.add(s);
        }
        out.add("sink");
        return out;
    }
    /**
     * Generates a density of the obtained PH distribution.
     * 
     * @param histo histogram with input samples
     * @param xOffset offset of the x data in the histogram, -1 for taking real middle x
     * instead
     * @param em distribution
     * @param outDensity output stream to write the data, is not closed by this
     * method
     */
    protected static void generateDensity(double[][] histo,
            double xOffset, OlssonEM em,
            OutputStream outStream) {
        PrintWriter out = new PrintWriter(outStream);
        double prevX = -1;
        for(int row = 0; row < histo.length; ++row) {
            double x;
            if(row == 0)
                x = histo[row][0];
            else if(row == histo.length - 1)
                x = histo[row][1];
            else if(xOffset == -1)
                x = (histo[row][0] + histo[row][1])/2.0;
            else
                x = histo[row][0] + xOffset;
            if(prevX == -1)
                out.println(x + " " + em.getDensity(x));
            else {
                final int STEPS = 5;
                double step = (x - prevX)/STEPS;
                for(int i = 1; i <= STEPS; ++i) {
                    double p = prevX + i*step;
                    out.println(p + " " + em.getDensity(p));
                }
            }
            prevX = x;
        }
        out.flush();
        if(out.checkError())
            throw new RuntimeException("could not write output file");
    }
    static enum ApproxMode {
        // if an edge point, just copy its value outerwards
        EDGE_FLAT,
        // like EDGE_FLAT, but on the lower edge interpolate frm 0
        START_ZERO,
    }
    private static double[][] approx(double[][] xxy, int steps,
            ApproxMode mode) {
        final int NUM = xxy.length;
        double[][] approx = new double[NUM*steps][3];
        final double MID = (steps - 1)/2.0;
        final double EPSILON = 1e-6;
        for(int i = 0; i < NUM; ++i) {
            double[] sample = xxy[i];
            double curr = sample[2];
            double prev;
            if(mode == ApproxMode.START_ZERO && i == 0)
                prev = 0;
            else
                prev = (xxy[Math.max(0, i - 1)][2] + curr)/2.0;
            double next = (xxy[Math.min(NUM - 1, i + 1)][2] + curr)/2.0;
            for(int s = 0; s < steps; ++s) {
                double width = sample[1] - sample[0];
                double widthH = width/2.0;
                double aX = (s + 0.5)*width/steps;
                double x = sample[0] + aX;
                double y;
                if(Math.abs(s - MID) < EPSILON)
                    y = curr;
                else if(s < MID)
                    y = prev + aX*(curr - prev)/(widthH);
                else
                    y = curr + (aX - widthH)*(next - curr)/(widthH);
                double radius = width/steps/2.0;
                approx[i*steps + s][0] = x - radius;
                approx[i*steps + s][1] = x + radius;
                approx[i*steps + s][2] = y;
            }
        }
        return approx;
    }
    public static void main(String[] args) throws IOException {
        String[] args_ = {
            //"../../test/przerwa_1.txt",
            //"czasa1.txt",
            "/home/art/modelling/prism/bins/bin8.txt:4z",
            //"0.023*/home/art/modelling/prism/bins/packet_size.txt",
            //"x",
            "-1", //"0.5",
            "8",
            "50",
            "bin1.nm",
            "density.txt",
        };
        // args = args_;
        if(args.length != 5 && args.length != 6)
            System.out.println(
                    "Fit to a probability density function using a PH-type distribution\n" +
                    "syntax: [<scale x>*]<input text file>[:<supersample>[z]] <x align> <phases> <iterations> " +
                    "( <output text file> | <output nm file> ) " +
                    "[ <output density file> ]\n" +
                    "align examples: -1 -- none (if data has x min, x max), " +
                            "0 -- x defines section's x min, 0.5 -- x defines section center");
        else {
            String inFileName = args[0];
            int supersample;
            boolean supersampleZero = false;
            int pos = inFileName.lastIndexOf(":");// 0.023
            if(pos == -1)
                supersample = 1;
            else {
                char c = inFileName.charAt(inFileName.length() - 1);
                if(!Character.isDigit(c)) {
                    switch(c) {
                        case 'z':
                            supersampleZero = true;
                            break;
                            
                        default:
                            throw new IOException("unknown supersample modifier: " + c);
                    }
                    inFileName = inFileName.substring(0, inFileName.length() - 1);
                }
                supersample = Integer.parseInt(inFileName.substring(pos + 1));
                inFileName = inFileName.substring(0, pos);
            }
            double scaleX;
            pos = inFileName.indexOf("*");// 0.023
            if(pos == -1)
                scaleX = 1.0;
            else {
                scaleX = Double.parseDouble(inFileName.substring(0, pos));
                inFileName = inFileName.substring(pos + 1);
            }
            double align = Double.parseDouble(args[1]);
            int phases = Integer.parseInt(args[2]);
            int iterations = Integer.parseInt(args[3]);
            String outFileName = args[4];
            String outDensityFileName;
            if(args.length > 5)
                outDensityFileName = args[5];
            else
                outDensityFileName = null;
            FileInputStream in = new FileInputStream(inFileName);
            FileOutputStream out = new FileOutputStream(outFileName);
            FileOutputStream outDensity = outDensityFileName == null ?
                    null : new FileOutputStream(outDensityFileName);
            double[][] histo = NumStream.toArray(in);
            if(scaleX != 1.0) {
                System.out.println("scaling X by " + scaleX);
                for(int i = 0; i < histo.length; ++i) {
                    double[] row = histo[i];
                    for(int x = 0; x < row.length - 1; ++x)
                        row[x] *= scaleX;
                }
            }
            if(supersample != 1) {
                System.out.println("supersampling by " + supersample);
                ApproxMode approxMode = ApproxMode.EDGE_FLAT;
                if(supersampleZero)
                    approxMode = ApproxMode.START_ZERO;
                histo = approx(histo, supersample, approxMode);
            }
//NumStream.fromArray(histo, outDensity);
//outDensity.close();System.exit(0);
            if(histo[0].length == 3 && align != -1)
                throw new RuntimeException("data contains xmin/xmax, no align needed");
            //final double RIDGE = 0.006*0;
            //histo[0][1] -= RIDGE;
            //histo[1][1] += RIDGE;
            double width = histo[1][0] - histo[0][0];
            OlssonEM.PHType phType = OlssonEM.PHType.COX;
            Xxy[] mass = ArrayDist.copyMass(
                    histo, align == -1 ? align : width*align, align == -1 ? align : width*(1.0 - align),
                    true, null);
            OlssonEM em = new OlssonEM(mass,
                    phType, phases, iterations);
            ArrayDist ad = new ArrayDist(mass, true);
            System.out.println("\nmean:    \torig = " + ad.getMean() +
                    "\tapprox = " + em.getMean());
            System.out.println("variance:\torig = " + ad.getVariance() +
                    "\tapprox = " + em.getVariance());
            pos = outFileName.lastIndexOf('.');
            String name;
            String extension;
            if(pos == -1) {
                name = outFileName;
                extension = "";
            } else {
                name = outFileName.substring(0, pos);
                extension = outFileName.substring(pos + 1).
                        toLowerCase();
            }
            double[][] matrix = em.getMatrix();
            if(extension.equals("nm")) {
                List<String> rowComments;
                if(phType == OlssonEM.PHType.COX)
                    rowComments = getRowComments(matrix);
                else
                    rowComments = null;
                MatrixIO.generateNm(name, out, matrix,
                        "approximation of \"" + inFileName + "\", type " +
                        phType.toString() + "_" + phases + ", " +
                        iterations + " iterations",
                        rowComments);
            } else if(extension.equals("txt")) {
                MatrixIO.generateText(out, matrix);
            } else
                throw new IOException("unknown extension of the output file: " +
                        "`" + extension + "', should be either `txt' or `nm'");
            if(outDensity != null) {
                System.out.print("\ngenerating probability density... ");
                System.out.flush();
                generateDensity(histo,  align == -1 ? -1 : width/2 - width*align,
                        em, outDensity);
                System.out.println("ok.\n");
            }
            if(outDensity != null)
                outDensity.close();
            out.close();
            in.close();
        }
    }
}
