set terminal pdfcairo monochrome dashed
set output 'tcp_size.pdf'
set xtics 250

set key bottom right
set size ratio 0.7
set xlabel "packet size [bytes]"
set ylabel "cdf"

s=1.14*1000
s=1.14*1000
t=1.15/1.140
t=1
plot [] "tcp_size.txt" using ($1*t):($2) with lines lw 2 title "caida 2016", \
	"density-200.cdf" using ($1*s):($2) with lines title "branches = 3, ph max = actual = 200", \
	"density-300.cdf" using ($1*s):($2) with lines title "branches = 3, ph max = actual = 300", \
	"density-800.cdf" using ($1*s):($2) with lines title "branches = 3, ph max = actual = 800", \
	"density-1600.cdf" using ($1*s):($2) with lines title "branches = 3, ph max = actual = 1600", \
	"density-3000.cdf" using ($1*s):($2) with lines title "branches = 3, ph max = 3000; actual = 2274", \
	"density-4.3000.cdf" using ($1*s):($2) with lines lw 2 title "branches = 4, ph max = 3000; actual = 3000"

set output 'tcp_size_300.pdf'
set key at 1410,0.8
#set key top left
#set grid xtics ytics
set y2label "error                                        "
set y2range [-0.2:0.8]
set ytics nomirror
set y2tics nomirror -0.2,0.2,0.2
set xrange [:1750]
plot [] "tcp_size.txt" using ($1*t):($2) with lines lw 2 title "caida 2016", \
	"density-300.cdf" using ($1*s):($2) with lines lw 3 title "branches = 3, ph max = actual = 300", \
	"tcp_size_diff.txt" using ($1*t):($2) axes x1y2 with lines lt 0 lw 4 title "error", \
	0 axes x1y2 with lines lt 1 lw 2 notitle

