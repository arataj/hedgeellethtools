cd $1
if [ -f "build.xml" ]; then
    ant clean >/dev/null
fi
FILE=../archive/$1
rm -rf $FILE
svn export -q . $FILE && cd ../archive/ &&
	tar cJf $1.tar.bz2 $1 && cd - &&
	rm -rf $FILE
#echo scp $FILE.tar.gz
# scp $FILE.tar.bz2 arataj@atos://home/arataj/archive/ &
# scp $FILE.tar.bz2 art@tesla://home/art/archive/
cd ..
