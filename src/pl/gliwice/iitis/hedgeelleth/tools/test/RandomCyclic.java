/*
 * RandomCyclic.java
 *
 * Created on May 21, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.tools.test;

import java.io.*;

import pl.gliwice.iitis.hedgeelleth.tools.MatrixIO;

/**
 * Denerates a random cyclic CTMC with an absorbing state.
 * 
 * @author Artur Rataj
 */
public class RandomCyclic {
    public static void main(String[] args) throws IOException {
        String[] args_ = {
            "true",
            "10",
            "a.nm",
        };
        // args = args_;
        if(args.length != 3)
            System.out.println("syntax: <cyclic> <states> <output nm file>");
        else {
            boolean cyclic = Boolean.parseBoolean(args[0]);
            int states = Integer.parseInt(args[1]);
            String outFileName = args[2];
            FileOutputStream out = new FileOutputStream(outFileName);
            double[][] matrix = new double[states][];
            for(int row = 0; row < states; ++row) {
                matrix[row] = new double[states];
                if(row < states - 1)
                    for(int col = 0; col < states; ++col) {
                        if(Math.random() < 0.1 &&
                                (cyclic || col > row))
                            matrix[row][col] = Math.pow(Math.random(), 3.0);
                    }
                else
                    matrix[row][states - 1] = 1.0;
            }
            int pos = outFileName.lastIndexOf('.');
            String name;
            if(pos == -1)
                name = outFileName;
            else
                name = outFileName.substring(0, pos);
            MatrixIO.generateNm(name, out, matrix, null, null);
            out.close();
        }
    }
}
