#
# retrieves a probability density graph from prism of reaching
# the absorbing state at a given time
#
# syntax: <model file> <min> <max> <step> <output file>
#
SCRIPTD=`dirname $BASH_SOURCE`
$SCRIPTD/run-PDensity.sh $2 $3 $4 density.pctl
prism $1 density.pctl -exportresults tmp.out
awk '{if (count++%4==2) print $0;}' tmp.out > tmp2.out
MIN=`echo $2 + $4/2 |bc -l`
MAX=`echo $3 - $4/2 |bc -l`
$SCRIPTD/run-AddX.sh tmp2.out $MIN $MAX $5
