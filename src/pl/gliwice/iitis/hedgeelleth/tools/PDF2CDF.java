/*
 * PDF2CDF.java
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.tools;

import java.util.*;
import java.io.*;

import pl.gliwice.iitis.hedgeelleth.compiler.util.text.NumStream;

/**
 * Converts a discrete PDF to a CDF.
 * 
 * @author Artur Rataj
 */
public class PDF2CDF {
    public static void main(String[] args) throws IOException {
        String[] args_ = {
            "/home/art/projects/lte/fig3-duration.csv",
            "/home/art/projects/lte/pdf-duration.txt",
            "4", "3",
        };
        // args = args_;
        if(args.length < 2 || args.length > 4) {
            System.out.println("arguments: <input file> <output file> [<distance between compared points>] [<smoothing iterations>]");
            System.out.println("The defaults are distance is 2 and no smoothing.");
        }
        FileInputStream in = new FileInputStream(args[0]);
        double[][] pdf = NumStream.toArray(in);
        in.close();
        if(pdf[0].length != 2)
            throw new IOException("a pair of numbers per line expected");
        double[][] cdf = PDF.cdf(pdf, true);
        FileOutputStream out = new FileOutputStream(args[1]);
        NumStream.fromArray(cdf, out);
        out.close();
    }
}
