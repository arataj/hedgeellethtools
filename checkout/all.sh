#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
$DIR/hedgeelleth-util.sh && \
$DIR/hedgeelleth.sh && \
$DIR/hedgeelleth-gpu.sh && \
$DIR/pta.sh && \
$DIR/olymp.sh && \
$DIR/hedgeelleth-compiler.sh
