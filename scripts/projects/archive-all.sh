#!/bin/bash
rm -f archive/*.bz2

./archive.sh SoundUtils $* && \
./archive.sh FrenchUtils $* && \
./archive.sh Papuga $* && \
./archive.sh Traductaid $* && \
./archive.sh HedgeellethCompiler $* && \
./archive.sh HedgeellethCPP $* && \
./archive.sh HedgeellethJava $* && \
./archive.sh Mirela $* && \
./archive.sh HedgeellethPTA $* && \
./archive.sh HedgeellethTADD $* && \
./archive.sh HedgeellethTools $* && \
./archive.sh HedgeellethTranslator $* && \
./archive.sh HedgeellethUtilities $* && \
./archive.sh HedgeellethVerics $* && \
./archive.sh HedgeellethVs $* && \
./archive.sh HedgeellethIDE $* && \
./archive.sh zeroHiperTxt $* && \
#./archive.sh doc $* && \
#./archive.sh lib $*

ARCHIVE=verics-$(date +%Y%m%d).tar
cd archive
tar cvf $ARCHIVE *.tar.bz2
#scp $ARCHIVE art@212.106.181.11://home/art/
cd ..
echo ARCHIVE ALL DONE
