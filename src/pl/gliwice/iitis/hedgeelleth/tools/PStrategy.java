/*
 * PStrategy.java
 *
 * Created on May 12, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */
package pl.gliwice.iitis.hedgeelleth.tools;

import java.util.*;
import java.io.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.CompilerUtils;

/**
 * Loads a strategy from prism-games. This is the new format of the strategy files,
 * and for a DTMC it needs to be couples with the transition matrix, e.g. by using
 * <code>PAdversary<code>.
 * 
 * @author Artur Rataj
 */
public class PStrategy {
    /**
     * Contains choice names, determines their indexes.
     */
    public final List<String> NAME;
    /**
     * The choice states, key is state, value is choice name index.
     */
    public final SortedMap<Long, Integer> CHOICE;
    
    /**
     * Creates a new strategy.
     * 
     * @param states set of states
     * @param traFilename name of the file with an adversary
     */
    public PStrategy(PStates states, String traFilename) throws IOException {
        NAME = new LinkedList<>();
        CHOICE = new TreeMap<>();
        InputStream in = new FileInputStream(traFilename);
        Scanner scLine = CompilerUtils.newScanner(in);
        long lineCount = 0;
        System.out.println("loading strategy...");
        while(scLine.hasNextLine()) {
            ++lineCount;
            final int LINE_M = 1000000;
            if(lineCount%(LINE_M + 1) == LINE_M)
                System.out.println(lineCount/LINE_M + "m");
            String line = scLine.nextLine();
            if(line.isEmpty())
                continue;
            Scanner scNumbers = CompilerUtils.newScanner(line).useDelimiter(" ");
            if(!scNumbers.hasNextLong())
                throw new IOException(traFilename + ": " + lineCount +
                        ": missing state index");
            long state = scNumbers.nextLong();
            if(!scNumbers.hasNext())
                throw new IOException(traFilename + ": " + lineCount +
                        ": missing choice name");
            String choice = scNumbers.next();
            if(!choice.equals("-")) {
                if(!NAME.contains(choice))
                    NAME.add(choice);
                CHOICE.put(state, NAME.indexOf(choice));
            }
        }
        System.out.println("loaded " + CHOICE.size() + " choices, ok.");
    }
}
