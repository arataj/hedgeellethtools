/*
 * GnuplotBodePlot.java
 *
 * Created on Apr 10, 2018
 *
 * Copyright (c) 2018  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.tools.plot;

import java.util.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;

import pl.gliwice.iitis.hedgeelleth.compiler.util.For;
import pl.gliwice.iitis.hedgeelleth.compiler.util.text.NumStream;

/**
 * Builds a Bode plot out of a number of frequency domain representations.
 * 
 * @author Artur Rataj
 */
public class GnuplotBodePlot {
    /**
     * Number of >F_0 sines to sum as harmonics.
     */
    final static int HARMONICS_NUM = 10;
    /**
     * A result on an analysis of a single FDR.
     */
    public static class FDRAnalyze {
        public final double AMP;
        public final double PHASE;
        public final double H_AMP;
        
        /**
         * @param array 0 for the const signal, then subsequent frequencies, meaning of columns
         * as defined in <code>ampCol</code>, <code>phaseCol</code>
         * @param ampCol number of the amplitude column, since 1 for the 1st column
         * @param phaseCol number of the phase column, since 1 for the 1st column
         * @param ampMult amplitude multiplier, both base and harmonics
         * @param phaseOffset phase offset, e.g. pi/2 of the stimulus is a sine
         * @param oddHarmonicsNum instead of the f0 sine (== 0) use a number of odd harmonics
         * which which approximate a pulse waveform (e.g. == 2); this list contains the number of
         * harmonics for subseuquent lists of FDRs
         */
        public FDRAnalyze(double[][] array, int ampCol, int phaseCol,
                double ampMult, double phaseOffset, int oddHarmonicsNum) {
            double ampSin = 0.0;
            double ampCos = 0.0;
            for(int oh = 0; oh <= oddHarmonicsNum; ++oh) {
                double a = array[1 + oh*2][ampCol - 1];
                double p = array[1 + oh*2][phaseCol - 1];
                ampSin += a*Math.sin(p);
                ampCos += a*Math.cos(p);
            }
            AMP =  Math.sqrt(ampSin*ampSin + ampCos*ampCos)*ampMult;
            double p = Math.atan2(ampSin, ampCos)+ phaseOffset;
            while(p > Math.PI)
                p -= 2.0*Math.PI;
            while(p < -Math.PI)
                p += 2.0*Math.PI;
            if(oddHarmonicsNum == 0) {
                double q = array[1][phaseCol - 1] + phaseOffset;
                while(q > Math.PI)
                    q -= 2.0*Math.PI;
                while(q < -Math.PI)
                    q += 2.0*Math.PI;
                if(Math.abs(p - q) > 1e-6)
                    throw new RuntimeException("unexpected");
            }
            PHASE = p;
            double ampH = 0;
            for(int j = 2; j < 2 + HARMONICS_NUM; ++j) {
                if(j%2 == 1) {
                    int harmIndex = (j - 1)/2;
                    if(harmIndex <= oddHarmonicsNum)
                        // ignore odd harmonics added to the base signal
                        continue;
                }
                ampH += Math.pow(array[j][ampCol - 1]*ampMult, 2.0);
            }
            H_AMP = Math.sqrt(ampH);
        }
    }
    /**
     * <p>A single frequency domain representation: an input file
     * description of columns and the associated frequency.</p>
     * 
     * <p>The rows conform to Fourier transform representation. e.g.
     * the 1st row is the DC, then <code>freq</code> etc.</p>
     */
    public static class FDR {
        /**
         * Base frequency, i.e. that of the sine represented by the 2nd
         * row.
         */
        public final double FREQ;
        /**
         * Amplitude multiplier.
         */
        public final double AMP_MULT;
        /**
         * Name of ther input file.
         */
        public final String FILENAME;
        /**
         * Number of the amplitude column, since 1 for the 1st column.
         */
        public final int AMP_COL;
        /**
         * Number of the phase column, since 1 for the 1st column.
         */
        public final int PHASE_COL;
        
        /**
         * 
         * @param freq base frequency, i.e. that of the sine represented by the 2nd row
         * @param ampMult amplitude multiplier
         * @param filename name of ther input file
         * @param ampCol number of the amplitude column, since 1 for the 1st column
         * @param phaseCol number of the phase column, since 1 for the 1st column
         */
        public FDR(double freq, double ampMult, String filename, int ampCol, int phaseCol) {
            FREQ = freq;
            AMP_MULT = ampMult;
            FILENAME = filename;
            AMP_COL = ampCol;
            PHASE_COL = phaseCol;
        }
    };
    public static class Range {
        final double MIN;
        final double MAX;
        final double SPAN;
        
        public Range(double min, double max) {
            MIN = min;
            MAX = max;
            SPAN = MAX - MIN;
        }
        @Override
        public String toString() {
            return "[" + MIN + ", " + MAX + "]";
        }
    }
    enum KeyMode {
        /* in AMP chart, no phase highlight */
        AMP,
        /* none, no phase highlight */
        NONE,
        /* none, phase highlight */
        NONE_PH,
        /* below the charts, phase highlight */
        BELOW,
        /* none but make the chart as high as if there were a key below */
        BELOW_NONE,
    }
    /**
     * Number of FDRs in the current list.
     */
    public int NUM;
    /**
     * Frequencies.
     */
    double[] FREQ;
    /**
     * Magnitudes.
     */
    double[] AMP;
    /**
     * Plot amplitude multipliers, does not affect the data file.
     */
    public double PLOT_AMP_MULT;
    /**
     * Phases.
     */
    double[] PHASE;
    /**
     * Range of frequencies in the FDRs and also in the output plot.
     */
    Range FREQ_RANGE;
    /**
     * User-choosen magnitude range.
     */
    Range AMP_RANGE;
    /**
     * User-choosen phase range.
     */
    Range PHASE_RANGE;
    List<Integer> phaseLoops;
    List<Integer> phaseLoopJumpP;
    List<Integer> phaseLoopJumpN;
    
    /**
     * Creates a set of two files: data and a gnuplot file.
     * 
     * @param listFdrs list of lists of subsequent (increasing frequencies) frequency domain representations;
     * for common scenarios <code>generateFdrs()</code> can be used; the first list determines the
     * frequency range
     * @param names name of the subsequent elements of <code>listFdrs</code>, empty string for
     * no name
     * @param phaseOffset phase offset, e.g. pi/2 of the stimulus is a sine
     * @param outPrefixGnuplot prefix of the gnuplot file
     * @param outPrefixData prefix of the output data generated by this method,
     * relatively to <code>outPrefixGnuplot</code>
     * @param outPrefixFigures prefix of the figures generated by gnuplot, relatively to
     * <code>outPrefixGnuplot</code>
     * @param freqRange frequency range of the plot, null of automatic
     * @param ampRange magnitude range of the plot
     * @param phaseRange phase range of the plot, null for -pi ... pi
     * @param plotAmpMult plot amplitude multiplier for subsequent lists, does not affect the data file
     * @param plotPulseHarmonicsNum see the definition at <code>FDRAnalyze</code>
     * @param xLabel if to add x (frequency) labels
     * @param yLabel if to add y (gain/phase) labels
     * @param keyMode key mode
     * @param keyAttr additional key attributes for customising key behaviour, if null then
     * <code>width -8 spacing 0.7 samplen 1.5</code>
     * @param plotEnable a triplet for each list of FDRS: plot amplitude, harmonics, phase, or null
     * if to plan all for all lists
     * @param extraWidth extra chart width
     * @param extraPhaseHeight extra height of the phase plot
     * @param customLineStyle if not null, replaces default line style: 4 styles for amp, 4 styles for
     * harmonics, 4 styles for phase
     */
    public GnuplotBodePlot(List<List<FDR>> listFdrs, List<String> names, double phaseOffset,
            String outPrefixGnuplot, String outPrefixData, String outPrefixFigures,
            Range freqRange, Range ampRange, Range phaseRange, List<Double> plotAmpMult,
            List<Integer> plotPulseHarmonicsNum,
            boolean xLabel, boolean yLabel, KeyMode keyMode, String keyAttr,
            List<Boolean> plotEnable, double extraWidth, double extraPhaseHeight,
            String customLineStyle) throws IOException {
        StringBuilder outAmp = new StringBuilder();
        StringBuilder outPhase = new StringBuilder();
        if(keyAttr == null)
            keyAttr = "width -8 spacing 0.7 samplen 1.5";
        int count = 0;
        for(List<FDR> fdrs : listFdrs) {
            NUM = fdrs.size();
            FREQ = new double[NUM];
            AMP = new double[NUM];
            PHASE = new double[NUM];
            if(freqRange != null)
                FREQ_RANGE = new Range(freqRange.MIN, freqRange.MAX);
            else
                FREQ_RANGE = new Range(fdrs.get(0).FREQ, fdrs.get(NUM - 1).FREQ);
            AMP_RANGE = ampRange;
            if(phaseRange == null)
                phaseRange = new Range(-Math.PI, Math.PI);
            PHASE_RANGE = phaseRange;
            PLOT_AMP_MULT = plotAmpMult.get(count);
            phaseLoops = new LinkedList<>();
            phaseLoopJumpP = new LinkedList<>();
            phaseLoopJumpN = new LinkedList<>();
            int oddHarmonicsNum = plotPulseHarmonicsNum.get(count);
            String outDir = outPrefixGnuplot.substring(0, outPrefixGnuplot.lastIndexOf(File.separatorChar) + 1);
            String outPrefixDataAbs = outDir + outPrefixData;
            printData(count, fdrs, phaseOffset, oddHarmonicsNum, outPrefixDataAbs);
            printPlot(count, listFdrs.size(), outAmp, outPhase, names.get(count),
                    outPrefixData, outPrefixFigures,
                    xLabel, yLabel, keyMode, keyAttr, plotEnable,
                    extraWidth, extraPhaseHeight, customLineStyle);
            ++count;
        }
        try(PrintWriter data = new PrintWriter(outPrefixGnuplot + ".gnuplot")) {
            data.print(outAmp.toString());
            data.print(outPhase.toString());
        }
    }
    /**
     * Creates a set of two files: data and a gnuplot file.
     * 
     * <p>This is a convenience constructor. For a description of all
     * parameters, see the main constructor.</p>
     * 
     * @param fdrs subsequent (increasing frequencies) frequency domain representations;
     * for common scenarios <code>generateFdrs()</code> can be used
     */
    public GnuplotBodePlot(List<FDR> fdrs, double phaseOffset,
            String outPrefixGnuplot, String outPrefixData, String outPrefixFigures,
            Range ampRange, Range phaseRange, double plotAmpMult,
            boolean xLabel, boolean yLabel, KeyMode keyMode) throws IOException {
        this(makeListFdrs(fdrs), makeListEmptyName(),
                phaseOffset, outPrefixGnuplot, outPrefixData, outPrefixFigures,
            null, ampRange, phaseRange, makeListAmp(plotAmpMult), makeListOH(),
            xLabel, yLabel, keyMode, null, null, 0.0, 0.0, null);
    }
    private static List<List<FDR>> makeListFdrs(List<FDR> fdrs) {
        List<List<FDR>> lists = new LinkedList<>();
        lists.add(fdrs);
        return lists;
    }
    private static List<String> makeListEmptyName() {
        List<String> list = new LinkedList<>();
        list.add("");
        return list;
    }
    private static List<Double> makeListAmp(double amp) {
        List<Double> list = new LinkedList<>();
        list.add(amp);
        return list;
    }
    private static List<Integer> makeListOH() {
        List<Integer> list = new LinkedList<>();
        list.add(0);
        return list;
    }
    /**
     * Prints data files.
     * 
     * @param dCount index of the current list of FDRs, 0 for the first one
     * @param fdrs subsequent (increasing frequencies) frequency domain representations;
     * for common scenarios <code>generateFdrs()</code> can be used
     * @param phaseOffset phase offset, e.g. pi/2 of the stimulus is a sine
     * @param oddHarmonicsNum number of odd harmonics to treat as a base signal
     * @param outPrefix prefix of the output data
     * @return rows where phase loops through pi, rows indexed from 0
     */
    private void printData(int dCount, List<FDR> fdrs, double phaseOffset, int oddHarmonicsNum,
            String outPrefix) throws IOException {
        int num = fdrs.size();
        // harmonics, possibly minus some odd harmonics
        double[] ampH = new double[num];
        For.eachIO(fdrs, (f, i) -> {
            String filename = f.FILENAME;
            if(!Files.exists(Paths.get(filename))) {
                // temporary hack
                int p = filename.lastIndexOf("_");
                filename = filename.substring(0, p) + ".0" + filename.substring(p);
                if(!Files.exists(Paths.get(filename))) {
                    int start = filename.lastIndexOf("FREQ") + 4;
                    int end = filename.lastIndexOf("_");
                    filename = filename.substring(0, start) + ((int)f.FREQ) + filename.substring(end);
                }
            }
            try(InputStream in = new FileInputStream(filename)) {
                double[][] array = NumStream.toArray(in);
                FREQ[i] = f.FREQ;
//                double a1 = array[1][f.AMP_COL - 1];
//                double a3 = array[3][f.AMP_COL - 1];
//                double a5 = array[5][f.AMP_COL - 1];
//                double p1 = array[1][f.PHASE_COL - 1];
//                double p3 = array[3][f.PHASE_COL - 1];
//                double p5 = array[5][f.PHASE_COL - 1];
                FDRAnalyze fdra = new FDRAnalyze(array, f.AMP_COL, f.PHASE_COL,
                        f.AMP_MULT, phaseOffset, oddHarmonicsNum);
                AMP[i] = fdra.AMP;
                PHASE[i] = fdra.PHASE;
                ampH[i] = fdra.H_AMP;
            }
        });
        double prevP = Double.NaN;
        for(int i = 0; i < NUM; ++i) {
            double p = PHASE[i];
            if(!Double.isNaN(prevP) && prevP*p < 0 &&
                    Math.abs(prevP) > Math.PI/2.0 && Math.abs(p) > Math.PI/2.0) {
                // a guess that it is a loop
                phaseLoops.add(i - 1);
            }
            prevP = p;
        }
        try(PrintWriter data = new PrintWriter(outPrefix + "_" + dCount + ".txt")) {
            final ListIterator<Integer> pli = phaseLoops.listIterator();
            For.each(FREQ, AMP, PHASE, ampH, (f, a, p, h, i) -> {
                if(phaseLoops.contains(i - 1)) {
                    boolean jumpP = false;
                    boolean jumpN = false;
                    if(phaseLoops.contains(i) && false) {
                        jumpP = true;
                    } else {
                        double q;
                        if(p > 0)
                            q = p - Math.PI*2.0;
                        else
                            q = p + Math.PI*2.0;
                        data.println(f + "\t" + a + "\t" + q + "\t" + h);
                        if(Math.abs(q) < Math.PI && i < PHASE.length - 1) {
                            double p2 = PHASE[i + 1];
                            if(p2 > 0)
                                q = p2 - Math.PI*2.0;
                            else
                                q = p2 + Math.PI*2.0;
                            if(Math.abs(q) >= Math.PI) {
                                data.println(FREQ[i + 1] + "\t" + AMP[i + 1] + "\t" + q + "\t" + ampH[i + 1]);
                                jumpP = true;
                            }
                        }
                        if(Math.abs(p) < Math.PI && i > 1) {
                            double q2 = PHASE[i - 1];
                            if(q2 > 0)
                                q = q2 - Math.PI*2.0;
                            else
                                q = q2 + Math.PI*2.0;
                            if(Math.abs(q) >= Math.PI) {
                                data.println(FREQ[i - 1] + "\t" + AMP[i - 1] + "\t" + q + "\t" + ampH[i - 1]);
                                jumpN = true;
                            }
                        }
                    }
                    phaseLoopJumpP.add(jumpP ? 1 : 0);
                    phaseLoopJumpN.add(jumpN ? 1 : 0);
                }
                data.println(f + "\t" + a + "\t" + p + "\t" + h);
            });
        }
        // update loops to inserted extra lines
        final List<Integer> t = new LinkedList<>();
        int count = 1;
        for(Integer i : phaseLoops) {
            t.add(i + count);
            ++count;
        }
        phaseLoops.clear();
        phaseLoops.addAll(t);
    }
//    private String generatePlotText() {
//        String preamble = "";
//    }
    /**
     * Print the gnuplot file.
     * 
     * @param dCount index of the current list of FDRs, 0 for the first one; if a
     * successive list, only another set of lines is added to the existing plot
     * @param dTotal number of lists of FDRs, 1 for a single such list
     * @param phaseLoops rows where phase loops through pi, rows indexed from 0
     * @param outAmp builder of the amp/common part
     * @param outPhase builder of the phase part
     * @param outPrefixData prefix of the output data generated by this method,
     * relatively to <code>outPrefixGnuplot</code>
     * @param outPrefixFigures prefix of the figures generated by gnuplot, relatively to
     * <code>outPrefixGnuplot</code>
     * @param xLabel if to add x (frequency) labels
     * @param yLabel if to add y (gain/phase) labels
     * @param keyMode key mode
     * @param plotEnable see the same parameter in the constructor
     * @param extraWidth extra chart width
     * @param extraPhaseHeight extra height of the phase plot
     * @param customLineStyle if not null, replaces default line style: 4 styles for amp, 4 styles for
     * harmonics, 4 styles for phase
     */
    private void printPlot(int dCount, int dTotal, StringBuilder outAmp, StringBuilder outPhase,
            String name, String outPrefixData, String outPrefixFigures,
            boolean xLabel, boolean yLabel, KeyMode keyMode,
            String keyAttr, List<Boolean> plotEnable,
            double extraWidth, double extraPhaseHeight, String customLineStyle) throws IOException {
        final int STYLE_NUM = 5;
        boolean enableAmp;
        if(plotEnable == null)
            enableAmp = true;
        else
            enableAmp = plotEnable.get(dCount*3 + 0);
        boolean enableH;
        if(plotEnable == null)
            enableH = true;
        else
            enableH = plotEnable.get(dCount*3 + 1);
        boolean enablePhase;
        if(plotEnable == null)
            enablePhase = true;
        else
            enablePhase = plotEnable.get(dCount*3 + 2);
        if(!name.isEmpty())
            name = name + ", ";
        outPrefixData += "_" + dCount;
        boolean newPlot = dCount == 0;
        boolean willBeContinued = dCount < dTotal - 1;
        boolean HIGH = false;
        String ampY, ampO, phaseO;
        double phaseY;
        if(HIGH) {
            ampY = "3.5";
            phaseY = 3.0;
            ampO = "0.7";
            phaseO = "2.7";
        } else {
            ampY = "2.5";
            phaseY = 2.7;
            ampO = "0.9";
            phaseO = "2.1";
        }
        boolean belowNone = keyMode == KeyMode.BELOW_NONE;
        if(belowNone)
            keyMode = KeyMode.BELOW;
        double extraBmargin = keyMode == KeyMode.BELOW ? Math.max(0, (dTotal - 3)*0.4) : 0;
        if(keyMode == KeyMode.BELOW)
            phaseY += 0.8 + extraBmargin*0.9;
        phaseY += extraPhaseHeight;
        if(!xLabel)
            phaseY -= 0.35;
        double width = 7.6 + extraWidth;
        if(!yLabel)
            width -= 0.9;
        String keyPosAmp = null;
        String keyPosPhase = null;
        switch(keyMode) {
            case AMP:
                keyPosAmp = "set key bottom left maxcols 2 ";
                keyPosPhase = "unset key";
                break;

            case NONE:
            case NONE_PH:
                keyPosAmp = "unset key";
                keyPosPhase = "unset key";
                break;

            case BELOW:
                keyPosAmp = "unset key";
                keyPosPhase = "set key below left maxcols 2 ";
                break;

        }
        String baseFreq = "ls " + (1 + dCount) + " " + (belowNone ? "notitle" : "title \"" + name + "\\\\(F_0\\\\)\"");
        String harmonics = "ls " + (1 + STYLE_NUM + dCount) + " " + (belowNone ? "notitle" : "title \"" + name + "\\\\(H_{\\\\le" + HARMONICS_NUM + "}\\\\)\"");
        boolean highlightPhase =
                keyMode == KeyMode.NONE_PH ||
                keyMode == KeyMode.BELOW ||
                dTotal > 1;
        if(newPlot) {
            String lineStyle;
            if(customLineStyle != null)
                lineStyle = customLineStyle;
            else
                lineStyle =
                    "set style line 1 lt 1 lw 2 lc rgb '#ff0000'\n" +
                    "set style line 2 lt 1 lw 2 lc rgb '#a0a000'\n" +
                    "set style line 3 lt 3 lw 2 lc rgb '#a0a000'\n" +
                    "set style line 4 lt 3 lw 3 lc rgb '#808080'\n" +
                    "set style line 5 lt 3 lw 3 lc rgb '#0080ff'\n" +
                    "set style line 6 lt 3 lc rgb '#0000ff'\n" +
                    "set style line 7 lt 3 lc rgb '#00a0a0'\n" +
                    "set style line 8 lt 3 lc rgb '#a000a0'\n" +
                    "set style line 9 lt 3 lc rgb '#909090'\n" +
                    "set style line 10 lt 3 lc rgb '#40a0ff'\n" +
                    "set style line 11 lt 1 lw 2 lc rgb '#002080'\n" +
                    "set style line 12 lt 1 lw 2 lc rgb '#40a020'\n" +
                    "set style line 13 lt 3 lw 2 lc rgb '#40a020'\n" +
                    "set style line 14 lt 3 lw 3 lc rgb '#604060'\n" +
                    "set style line 15 lt 3 lw 3 lc rgb '#0080ff'\n";
            outAmp.append(
                "set terminal cairolatex size " + width + "cm," + ampY + "cm dashed \\\n" +
                "	input font 'ptm,bx,8pt' fontscale 0.65 header \"\\\\scriptsize\"\n" +
                       (keyPosAmp.contains("unset") ?
                               keyPosAmp + "\n" :
                       (HIGH ?
                            keyPosAmp + keyAttr + "\n" :
                            keyPosAmp + keyAttr + "\n"
                       )) +
                       "set xrange [" + FREQ_RANGE.MIN + ":" + FREQ_RANGE.MAX + "]\n" +
                "set logscale x\n" +
                "set grid\n" +
                "set lmargin " + (yLabel ? 11 : 6.7) + "\n" +
                "\n" +
                "set output \"" + outPrefixFigures + "-amp.tex\"\n" +
                "unset xlabel\n" +
                "set xtics (\"\" 10, \"\" 30, \"\" 100, \"\" 300, \"\" 1000, \"\" 3000)\n" +
                       (yLabel ?
                            "set ylabel \"" +
                                (HIGH ? "Magnitude" : "Magn.") + " (dB)\\\\vspace{-" + ampO + "cm}\"\n" +
                            (HIGH ?
                                 "set ytics (\"-25\" 0.0031623, \"-20\" 0.01, \"-15\" 0.031623, \"-10\" 0.1, \"-5\" 0.31623, \" 0\" 1.0, \"5\" 3.1623, \"10\" 10)\n" :
                                 "set ytics (\"-20\" 0.01, \"-10\" 0.1, \" 0\" 1.0, \"10\" 10)\n"
                            ) :
                            "unset ylabel\n" +
                            (HIGH ?
                                 "set ytics (\"\" 0.0031623, \"\" 0.01, \"\" 0.031623, \"\" 0.1, \"\" 0.31623, \"\" 1.0, \"\" 3.1623, \"\" 10)\n" :
                                 "set ytics (\"\" 0.01, \"\" 0.1, \"\" 1.0, \"\" 10)\n"
                            )
                       ) +
                "set logscale y\n" +
                "set yrange [" + AMP_RANGE.MIN + ":" + AMP_RANGE.MAX + "]\n" +
                lineStyle +
                "plot \\\n");
        }
        outAmp.append(
               (enableAmp ?
                    "	\"" + outPrefixData + ".txt\" using 1:($2*" + PLOT_AMP_MULT + ") smooth cspline " + baseFreq + ", \\\n" :
                    "         -1, \\\n"
               ) +
               (enableH ?
                    "	\"" + outPrefixData + ".txt\" using 1:($4*" + PLOT_AMP_MULT + ") smooth cspline " + harmonics :
                    "         -1"
               )
        );
        if(willBeContinued)
            outAmp.append(", \\\n");
        else
            outAmp.append("\n");
        if(newPlot) {
            outPhase.append(
	"\n" +
	"set terminal cairolatex size " + width + "cm," + phaseY + "cm dashed \\\n" +
	"	input font 'ptm,bx,8pt' fontscale 0.65 header \"\\\\scriptsize\"\n" +
               (keyPosPhase.contains("unset") ?
                       keyPosPhase + "\n" :
                       keyPosPhase + keyAttr + "\n"
               ) +
	"set output \"" + outPrefixFigures + "-phase.tex\"\n" +
               (xLabel ?
                    "set xlabel \"Frequency (Hz)\\\\vspace{0.5cm}\"\n" +
                    "set xtics (1, 3, 10, 30, 100, 300, 1000, 3000, 10000)\n" :
                    "unset xlabel\n" +
                    "set xtics (\"\" 1, \"\" 3, \"\" 10, \"\" 30, \"\" 100, \"\" 300, \"\" 1000, \"\" 3000, \"\" 10000)\n"
               ) +
               (yLabel ?
                    "set ylabel \"Phase (rad)\\\\vspace{-" + phaseO + "cm}\"\n" +
                    (HIGH ?
                         "set ytics (\"\\\\(-\\\\pi\\\\)\" -pi, \"\\\\(-\\\\frac{\\\\pi}{2}\\\\)\" -pi/2, \" 0\" 0, \"\\\\(\\\\frac{\\\\pi}{2}\\\\)\" pi/2, \"\\\\(\\\\pi\\\\)\" pi)\n" :
                         "set ytics (\"\\\\(-\\\\pi\\\\)\" -pi, \" 0\" 0, \"\\\\(\\\\pi\\\\)\" pi)\n"
                    ) :
                    "unset ylabel\n" +
                    (HIGH ?
                         "set ytics (\"\" -pi, \"\" -pi/2, \"\" 0, \"\" pi/2, \"\" pi)\n" :
                         "set ytics (\"\" -pi, \"\" 0, \"\" pi)\n"
                    )
               ) +
	"set nologscale y\n" +
	"set yrange [" + PHASE_RANGE.MIN + ":" + PHASE_RANGE.MAX + "]\n" +
               (keyMode == KeyMode.BELOW ?
                       "set bmargin " + (5 + extraBmargin*2) + "\n" : ""
               ) +
	"plot \\\n"
            );
        }
        if(keyMode == KeyMode.BELOW && enableAmp)
            outPhase.append(
                    "\t-10 " + baseFreq + ", \\\n"
            );
        if(keyMode == KeyMode.BELOW && enableH)
            outPhase.append(
                    "\t-10 " + harmonics + ", \\\n"
            );
        final int LAST = NUM - 1 + phaseLoops.size();
        phaseLoops.add(LAST);
        phaseLoopJumpP.add(0);
        phaseLoopJumpN.add(0);
        int prev = -1;
        int offset = 0;
        Iterator<Integer> jumpP = phaseLoopJumpP.iterator();
        Iterator<Integer> jumpN = phaseLoopJumpN.iterator();
        for(int curr : phaseLoops) {
            String approx;
            int jP = jumpP.next();
            int jN = jumpN.next();
            int offsetCurr = curr + offset + jP;
            prev += jP;
            offset += jP + jN;
            if(offsetCurr - prev <= 2)
                approx = "with lines";
            else
                approx = "smooth cspline";
            // ::0::32
            // ::33::38
            // ::39::43
            if(enablePhase)
                outPhase.append(
                        "\"" + outPrefixData + ".txt\" every ::" +
                        (prev + 1) + "::" + offsetCurr + " using 1:3 " + approx + " " +
                        (highlightPhase ? "ls " + (1 + 2*STYLE_NUM + dCount) : "lt 1 lw 2") + " " +
                        (prev == -1 && keyMode == KeyMode.BELOW && !belowNone ?
                                "title \"" + name + "\\\\(\\\\phi\\\\)~ \"" : "notitle"
                        ));
            if(curr < LAST) {
                if(enablePhase)
                    outPhase.append(", \\");
            } else {
                if(willBeContinued && enablePhase)
                    outPhase.append(", \\");
            }
            outPhase.append("\n");
            prev = offsetCurr;
        }
    }
    /**
     * A helper method for generating a set of filenames representing
     * a geometrical sequence of frequencies.
     * 
     * @param low the lowest frequency (that represented by the first
     * FDR)
     * @param mult multiplier; each following frequency is the preceeding
     * one multiplied by the value
     * @param num total number of FDRs
     * @param normHisto if false, all amplitude data is taken as is; otherwise
     * amplitude of frequency <code>f</code> it is multiplied by <code>low/f</code>
     * i.e. it is assumed that the original spatial representation was a histogram
     * normalised to have an area of 1 (like a PDF)
     * @param namePattern name pattern, with <code>*</code> for a frequency rounded to
     * integer
     * @param ampCol number of the amplitude column, since 1 for the 1st column
     * @param phaseCol number of the phase column, since 1 for the 1st column
     * @return a list of FDRs; empty if <code>num = 0</code>
     */
    public static List<FDR> generateFdrs(double low, double mult, int num,
            boolean normHisto, String namePattern, int ampCol, int phaseCol) {
        List<FDR> out = new LinkedList<>();
        int freqPos = namePattern.indexOf("*");
        String prefix = namePattern.substring(0, freqPos);
        String postfix = namePattern.substring(freqPos + 1);
        double freq = low;
        String prevName = null;
        for(int n = 0; n < num; ++n) {
            String name = prefix + Math.round(freq) + postfix;
            if(prevName != null && prevName.equals(name))
                throw new RuntimeException("duplicate name " + name);
            double ampMult;
            if(normHisto)
                ampMult = low/freq;
            else
                ampMult = 1.0;
            out.add(new FDR(freq, ampMult, name, ampCol, phaseCol));
            freq *= mult;
            prevName = name;
        }
        return out;
    }
    public static void gPlots() throws IOException {
        final String IN_PREFIX = "/home/art/projects/ngreen/NgreenOpt/data/";
        final String IN_SUFFIX = "_FREQ*_freqD0.txt";
        final String IN_SUFFIX_QT = "_FREQ*_qt_freqD0.txt";
        final String OUT_DIR = "/home/art/projects/ngreen/doc/ng-oa/";
        final String OUT_PREFIX_DATA = "data/fdrs_out_";
        final String OUT_PREFIX_FIGURES = "figures/bode-";
        final Range FREQ = new Range(30.517578125/4, 6569.504);
        final Range GAIN = new Range(0.005, 2.4);
        final Range PHASE = new Range(-Math.PI, Math.PI);
        double gain = -1;
        List<Integer> harmonics = new LinkedList<>();
        harmonics.add(0);
        harmonics.add(0);
        harmonics.add(0);
        harmonics.add(0);
        harmonics.add(0);
        boolean[] enableA = {
            // amp, h, phase
            true, false, true,
            true, false, true,
            true, false, true,
            true, false, true,
            true, false, true,
        };
        final double LOW_SIN = 30.517578125/4;
        final int NUM_SIN = 40;
        List<Boolean> enable = new LinkedList<>();
        for(boolean b : enableA)
            enable.add(b);
        boolean yLabel = true;
        for(int exp = 0; exp <= 2; ++exp) {
            boolean xLabel = exp == 2;
            KeyMode keyMode = exp == 2 ? KeyMode.BELOW : KeyMode.NONE;
            List<List<FDR>> listsFdrs = new LinkedList<>();
            List<String> names = new LinkedList<>();
            List<Double> gains = new LinkedList<>();
            String trafficStr;
            if(exp != 1)
                trafficStr = "MIXED";
            else
                trafficStr = "EXP";
            for(int load = 5; load <= 9; ++load) {
                if(load == 4)
                    continue;
                String loadStr = new DecimalFormat("#.######").format(load/10.0);
                final String PREFIX_2 = "L" + loadStr + "_" + trafficStr + "_SIN_AMP1.0";
                List<FDR> fdrs;
                if(exp == 2)
                    fdrs = generateFdrs(
                        LOW_SIN*4, Math.pow(2.0, 1.0/4), 24,
                        false,
                        IN_PREFIX + PREFIX_2 + IN_SUFFIX_QT,
                        2, 3);
                else
                    fdrs = generateFdrs(
                        LOW_SIN, Math.pow(2.0, 1.0/4), NUM_SIN,
                        false,
                        IN_PREFIX + PREFIX_2 + IN_SUFFIX,
                        2, 3);
                listsFdrs.add(fdrs);
                names.add("L=" + loadStr);
                if(exp == 0) {
                    switch(load) {
                        case 1:
                            gain = 1/17.0;
                            break;

                        case 4:
                            gain = 1/240.0;
                            break;

                        case 5:
                            gain = 1/240.0;
                            break;

                        case 6:
                            gain = 1/210.0;
                            break;

                        case 7:
                            gain = 1/220.0;
                            break;

                        case 8:
                            gain = 1/250.0;
                            break;

                        case 9:
                            gain = 1/350.0;
                            break;

                        case 10:
                            gain = 1/40.0;
                            break;

                    }
                } else {
                    switch(load) {
                        case 1:
                            gain = 1/17.0;
                            break;

                        case 4:
                            gain = 1/240.0;
                            break;

                        case 5:
                            gain = 1/240.0;
                            break;

                        case 6:
                            gain = 1/210.0;
                            break;

                        case 7:
                            gain = 1/150.0;
                            break;

                        case 8:
                            gain = 1/100.0;
                            break;

                        case 9:
                            gain = 1/50.0;
                            break;

                        case 10:
                            gain = 1/40.0;
                            break;

                    }
                }
                gains.add(gain);
            }
            if(exp == 0 && false) {
                int load = 7;
                String loadStr = new DecimalFormat("#.######").format(load/10.0);
                final String IN_SUFFIX_qt = "_FREQ*_freqD0.txt";
                final String PREFIX_qt = "L" + loadStr + "_" + trafficStr + "_SIN_AMP0.5";
                List<FDR> fdrs = generateFdrs(
                        30.517578125, Math.pow(2.0, 1.0/2), 12,
                        false,
                        IN_PREFIX + PREFIX_qt + IN_SUFFIX_qt,
                        2, 3);
                listsFdrs.add(fdrs);
                names.add("test");
                gains.add(1/100.0);
            }
            String SUFFIX = "g-bode-" + trafficStr;
            if(exp == 2)
                SUFFIX += "_qt";
            String lineStyle =
                    "set style line 1  lc rgb '#000000' lt 3 lw 2\n" +
                    "set style line 2  lc rgb '#33cc55' lt 3 lw 3\n" +
                    "set style line 3  lc rgb '#bbbb55' lt 3 lw 4\n" +
                    "set style line 4  lc rgb '#ee5580' lt 3 lw 5\n" +
                    "set style line 5  lc rgb '#8055ee' lt 3 lw 5\n" +
                        "set style line 6 lt 4\n" +
                        "set style line 7 lt 4\n" +
                        "set style line 8 lt 4\n" +
                        "set style line 9 lt 4\n" +
                        "set style line 10 lt 4\n" +
                    "set style line 11  lc rgb '#000000' lt 1 lw 2\n" +
                    "set style line 12  lc rgb '#33cc55' lt 1 lw 3\n" +
                    "set style line 13  lc rgb '#bbbb55' lt 1 lw 4\n" +
                    "set style line 14  lc rgb '#ee5580' lt 1 lw 5\n" +
                    "set style line 15  lc rgb '#8055ee' lt 1 lw 5\n";
            GnuplotBodePlot b = new GnuplotBodePlot(listsFdrs, names, Math.PI/2.0,
                    OUT_DIR + SUFFIX,
                    OUT_PREFIX_DATA + SUFFIX,
                    OUT_PREFIX_FIGURES + SUFFIX,
                    FREQ, GAIN, PHASE, gains, harmonics, xLabel, yLabel,
                    keyMode, null, enable, 0.5, 0.5, lineStyle);
        }
    }
    public static void plots() throws IOException {
        for(int load = 9; load <= 9; load += 1) {
//            if(load == 2 || load == 3 || load == 4 || load == 7)
//                continue;
            final String IN_PREFIX = "/home/art/projects/ngreen/NgreenOpt/data/";
            final String IN_SUFFIX = "_FREQ*_freqD0.txt";
            final String OUT_DIR = "/home/art/projects/ngreen/doc/ng-oa/";
            final String OUT_PREFIX_DATA = "data/fdrs_out_";
            final String OUT_PREFIX_FIGURES = "figures/bode-";
            String loadStr = new DecimalFormat("#.######").format(load/10.0 - 0.05);
            final String PREFIX_1 = "L" + loadStr + "_EXP_SIN_AMP1.0";
            final String PREFIX_2 = "L" + loadStr + "_MIXED_SIN_AMP1.0";
            final String PREFIX_FM_1 = "L" + loadStr + "_EXP_FMb11r27_AMP1.0";
            final String PREFIX_FM_2 = "L" + loadStr + "_MIXED_FMb11r27_AMP1.0";
            final Range GAIN = new Range(0.005, 2.4);
            final Range PHASE = new Range(-Math.PI, Math.PI);
            boolean xLabel = load == 10;
            double gain = -1;
            double gainFM = -1;
            List<Integer> harmonics = new LinkedList<>();
            harmonics.add(0);
            harmonics.add(0);
            harmonics.add(2);
            boolean[] enableA = {
                // amp, h, phase
                true, true, true,
                true, false, true,
                false, false, true,
            };
//            final double LOW_SIN = 7.62939453125;
//            final int NUM_SIN = 40;
//            final double LOW_FM = 122.0703125/2;
//            final int NUM_FM = 20;
            final double LOW_SIN = 30.517578125*8*Math.sqrt(2);
            final int NUM_SIN = 1;
            final double LOW_FM = LOW_SIN;
            final int NUM_FM = NUM_SIN;
            List<Boolean> enable = new LinkedList<>();
            for(boolean b : enableA)
                enable.add(b);
            // exp == 2 -> FM
            for(int exp = 0; exp <= 1; ++exp) {
                boolean yLabel = exp == 1 || exp == 2;
                KeyMode keyMode;
                if(load == 10) {
                    if(exp == 1)
                        keyMode = KeyMode.BELOW;
                    else
                        keyMode = KeyMode.BELOW_NONE;
                } else
                    keyMode = KeyMode.NONE_PH;
                switch(exp) {
                    case 1: {
                        List<FDR> fdrs = generateFdrs(
                                //30.517578125,
                                LOW_SIN, Math.pow(2.0, 1.0/4), NUM_SIN,
                                //122.0703125, Math.pow(2.0, 1.0/2), 12,
                                false,
                                IN_PREFIX + PREFIX_1 + IN_SUFFIX,
                                2, 3);
                        List<FDR> fdrsFM = generateFdrs(
                                LOW_FM, Math.pow(2.0, 1.0/4), NUM_FM,
                                false,
                                IN_PREFIX + PREFIX_FM_1 + IN_SUFFIX,
                                2, 3);
                        List<List<FDR>> listsFdrs = new LinkedList<>();
                        listsFdrs.add(fdrs);
                        listsFdrs.add(fdrsFM);
                        listsFdrs.add(fdrsFM);
                        List<String> names = new LinkedList<>();
                        names.add("sine");
                        names.add("FM");
                        names.add("FM+oh");
                        switch(load) {
                            case 1:
                                gain = 1/17.0;
                                break;

                            case 5:
                                gain = 1/240.0;
                                break;

                            case 6:
                                gain = 1/210.0;
                                break;

                            case 7:
                                gain = 1/150.0;
                                break;

                            case 8:
                                gain = 1/70.0;
                                break;

                            case 9:
                                gain = 1/50.0;
                                break;

                            case 10:
                                gain = 1/40.0;
                                break;

                        }
                        gainFM = gain;
                        List<Double> gains = new LinkedList<>();
                        gains.add(gain);
                        gains.add(gainFM);
                        gains.add(gainFM);
                        GnuplotBodePlot b = new GnuplotBodePlot(listsFdrs, names, Math.PI/2.0,
                                OUT_DIR + "bode-" + PREFIX_1,
                                OUT_PREFIX_DATA + PREFIX_1,
                                OUT_PREFIX_FIGURES + PREFIX_1,
                                null, GAIN, PHASE, gains, harmonics, xLabel, yLabel,
                                keyMode, null, enable, 0.0, 0.0, null);
                        break;
                    }
                    case 0: {
                        List<FDR> fdrs = generateFdrs(
                                //30.517578125,
                                LOW_SIN, Math.pow(2.0, 1.0/4), NUM_SIN,
                                false,
                                IN_PREFIX + PREFIX_2 + IN_SUFFIX,
                                2, 3);
                        List<FDR> fdrsFM = generateFdrs(
                                LOW_FM, Math.pow(2.0, 1.0/4), NUM_FM,
                                false,
                                IN_PREFIX + PREFIX_FM_2 + IN_SUFFIX,
                                2, 3);
                        List<List<FDR>> listsFdrs = new LinkedList<>();
                        listsFdrs.add(fdrs);
                        listsFdrs.add(fdrsFM);
                        listsFdrs.add(fdrsFM);
                        List<String> names = new LinkedList<>();
                        names.add("sine");
                        names.add("FM");
                        names.add("FM+oh");
                        switch(load) {
                            case 1:
                                gain = 1/14.0;
                                break;

                            case 5:
                                gain = 1/180.0;
                                break;

                            case 6:
                                gain = 1/180.0;
                                break;

                            case 7:
                                gain = 1/250.0;
                                break;

                            case 8:
                                gain = 1/310.0;
                                break;

                            case 9:
                                gain = 1/400.0;
                                break;

                            case 10:
                                gain = 1/550.0;
                                break;

                        }
                        gainFM = gain;
                        List<Double> gains = new LinkedList<>();
                        gains.add(gain);
                        gains.add(gainFM);
                        gains.add(gainFM);
                        GnuplotBodePlot b = new GnuplotBodePlot(
                                listsFdrs, names, Math.PI/2.0,
                                OUT_DIR + "bode-" + PREFIX_2,
                                OUT_PREFIX_DATA + PREFIX_2,
                                OUT_PREFIX_FIGURES + PREFIX_2,
                                null, GAIN, PHASE, gains, harmonics, xLabel, yLabel,
                                keyMode, null, enable, 0.0, 0.0, null);
                        break;
                    }
                }
            }
        }
    }
    public static void main(String[] args) throws IOException {
        if(true) {
            gPlots();
        } else {
            plots();
        }
    }
}
