/*
 * Race.java
 *
 * Created on Jun 1, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.tools.test;

/**
 *
 * @author Artur Rataj
 */
public class Race {
    static double c;
    public static void main_(String[] args) {
        final int NUM = 10000000;
        int aCount = 0;
        int bCount = 0;
        long start = System.currentTimeMillis();
        for(int i = 0; i < NUM; ++i) {
            /*
            double a = -Math.log(1.0 - Math.random())/4.0;
            double b = -Math.log(1.0 - Math.random())/2.0;
            if(a < b)
            */
            double a = Math.random();
            double b = Math.random();
            if(a < 1.0 - Math.pow(1.0 - b, 4.0/2.0))
                ++aCount;
            else
                ++bCount;
            c = -Math.log(1.0 - a)/4.0;
        }
        long stop = System.currentTimeMillis();
        System.out.println("as: " + (aCount*1.0/NUM));
        System.out.println("bs: " + (bCount*1.0/NUM));
        System.out.println("ratio a/b= " + (aCount*1.0/bCount) +
                "  b/a= " + (bCount*1.0/aCount));
        System.out.println("time " + (stop - start)/1000.0);
    }
    public static void main(String[] args) {
        final int NUM = 10000000;
        int aCount = 0;
        int bCount = 0;
        long start = System.currentTimeMillis();
        final double MAX = 3;
        final int SIZE = 300;
        int[][] pdf = new int[2][SIZE];
        for(int i = 0; i < NUM; ++i) {
            double t;
            /*
            double a = -Math.log(1.0 - Math.random())/4.0;
            double b = -Math.log(1.0 - Math.random())/2.0;
            if(a < b)
            */
            double a = Math.random();
            double b = Math.random();
            if(a < 1.0 - Math.pow(1.0 - b, 4.0/2.0)) {
                t = Math.min(MAX - 1e-9, -Math.log(1.0 - a)/4.0);
                ++pdf[0][(int)(t*SIZE/MAX)];
                ++aCount;
            } else {
                t = Math.min(MAX - 1e-9, -Math.log(1.0 - b)/2.0);
                ++pdf[1][(int)(t*SIZE/MAX)];
                ++bCount;
            }
        }
        long stop = System.currentTimeMillis();
        System.out.println("as: " + (aCount*1.0/NUM));
        System.out.println("bs: " + (bCount*1.0/NUM));
        System.out.println("ratio a/b= " + (aCount*1.0/bCount) +
                "  b/a= " + (bCount*1.0/aCount));
        System.out.println("time " + (stop - start)/1000.0);
        for(int i = 0; i < SIZE; ++i)
            System.out.println((i + 0.5)*MAX/SIZE + " " + pdf[1][i]*1.0/NUM);
    }
}
