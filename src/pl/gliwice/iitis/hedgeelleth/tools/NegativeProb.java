/*
 * NegativeProb.java
 *
 * Created on Dec 9, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.tools;

import java.util.*;
import java.io.*;

/**
 *
 * @author Artur Rataj
 */
public class NegativeProb {
    public static void main(String[] args) throws IOException {
        PStates states = new PStates("/home/art/modelling/prism/neg_prob/a.sta", null);
        PAdversary adv = new PAdversary(states,
                "/home/art/modelling/prism/neg_prob/a.tra", null);
        double[] p = adv.reachability(3);
        for(double e : p)
            System.out.print(" " + e);
        System.out.println();
        adv.toString();
    }
}
