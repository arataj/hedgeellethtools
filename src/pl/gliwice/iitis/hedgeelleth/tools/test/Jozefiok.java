/*
 * Jozefiok.java
 *
 * Created on Feb 14, 2017
 *
 * Copyright (c) 2017  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.tools.test;

import java.util.*;
import java.io.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.text.NumStream;
import pl.gliwice.iitis.hedgeelleth.tools.PDF;

/**
 *
 * @author Artur Rataj
 */
public class Jozefiok {
    public static void main(String[] args) throws IOException {
        final int DEBUG_LIMIT_NONE = Integer.MAX_VALUE/2;
        final int DEBUG_LIMIT = DEBUG_LIMIT_NONE;
//        final int DEBUG_LIMIT = 3;
        for(int A = 3; A <= 3; ++A) {
            //if(A == 3)
            //    continue;
            final String PATH = "/home/art/modelling/jozefiok/A" + A + "/";
            double[][] Pd = NumStream.toArray(new FileInputStream(PATH + "pd.txt"));
            PDF.checkOrdered(Pd);
            assert PDF.testMass(Pd, 0.002);
            double Ed = PDF.expected(Pd);
            double p1 = 1.0/(Ed + 1.0);
            double p2 = Ed/(Ed + 1.0);
            double[][] fp = NumStream.toArray(new FileInputStream(PATH + "fp.txt"));
            double[][] fd = NumStream.toArray(new FileInputStream(PATH + "fd.txt"));
            assert PDF.testMass(fp, 0.02);
            PDF.checkOrdered(fp);
            assert PDF.testMass(fd, 0.02);
            PDF.checkOrdered(fd);
            final int INTERP = 2500;
            double[][] fpU = PDF.trapezoid(fp, INTERP);
            double[][] fdU = PDF.trapezoid(fd, INTERP);
            double[][] fbU = PDF.add(PDF.mult(fpU, p1), PDF.mult(fdU, p2));
            double[][] FbU = PDF.cdf(fbU, false);
            final int K = 6;
            double[][] fbkU = PDF.mult(K, PDF.mult(PDF.pow(PDF.sub(1, FbU), K - 1), fbU));
            double[][] fbk = PDF.sparse(PDF.bucket(fbkU, INTERP));
            fpU = fdU = fbU = FbU = fbkU = null;
            // test
            PDF.testMass(fbk, 0.002);
            // 0 == 25, 1 == 50
            for(int QUEUE_VARIANT = 1; QUEUE_VARIANT <= 1; ++QUEUE_VARIANT) {
                int H;
                String postfix;
                switch(QUEUE_VARIANT) {
                    case 0:
                        postfix = "";
                        H = 25;
                        break;
                        
                    case 1:
                        postfix = "50";
                        H = 50;
                        break;
                        
                    default:
                        throw new RuntimeException("unexpected");
                }
                // 1 = simulation, 2 = MVA, 3 = Markov simple, 4 = diffusion, 5 = Markov complex
                for(int COLUMN = 5; COLUMN <= 5; ++COLUMN) {
                    System.out.println("COLUMN = " + COLUMN);
                    double[][] Pn = NumStream.toArray(
                            new FileInputStream(PATH + "pn" + postfix + ".txt"));
                    Pn = PDF.column(Pn, COLUMN);
                    PDF.testMass(Pn, 0.02);
                    PDF.checkOrdered(Pn);
                    Pn = PDF.normalize(Pn);
                    double sum = 0.0;
                    for(int n = 0; n <= K - 1; ++n)
                        sum += Pn[n][1];
                    double[][] frp = PDF.mult(sum, fp);
                    double[][] frd = PDF.mult(sum, fd);
                    double[][] fbkP = fbk;
                    for(int n = K; n <= Math.min(K + DEBUG_LIMIT, H - 1) ; ++n) {
                        System.out.println("N = " + n);
                        double[][] p = PDF.mult(Pn[n][1], PDF.convolute(fbkP, fp, true));
                        double[][] d = PDF.mult(Pn[n][1], PDF.convolute(fbkP, fd, true));
                        frp = PDF.add(frp, p);
                        frd = PDF.add(frd, p);
                        fbkP = PDF.convolute(fbkP, fbk, true);
                    }
                    if(DEBUG_LIMIT == DEBUG_LIMIT_NONE) {
                        PDF.testMass(frp, 0.02);
                        PDF.testMass(frd, 0.02);
                    }
                    PDF.print(PATH + "frp_n" + postfix + "_" + COLUMN + ".txt", frp);
                    PDF.print(PATH + "frd_n" + postfix + "_" + COLUMN + ".txt", frd);
                    double[][] fz = NumStream.toArray(new FileInputStream(PATH + "fz.txt"));
                    PDF.testMass(fz, 0.02);
                    PDF.checkOrdered(fz);
                    double[][] frSum = {{0, 0}};
                    double[][] fu = PDF.convolute(fz, frd, true);
                    // optimize
                    fu = PDF.sparse(fu);
                    PDF.testMass(fu, 0.02);
                    frp = PDF.sparse(frp);
                    double[][] fuP = {{0, 1}};
                    for(int r = 0; r < Math.min(DEBUG_LIMIT, Pd.length); ++r) {
                        System.out.println("R = " + r);
                        frSum = PDF.add(frSum, PDF.mult(Pd[r][1], fuP));
                        fuP = PDF.convolute(fuP, fu, true);
                        PDF.testMass(fuP, 0.05);
                    }
                    double[][] fr = PDF.convolute(frp, frSum, true);
                    if(DEBUG_LIMIT == DEBUG_LIMIT_NONE) {
                        PDF.testMass(frSum, 0.02);
                        PDF.testMass(fr, 0.02);
                    }
                    PDF.print(PATH + "fr_n" + postfix + "_" + COLUMN + ".txt", fr);
                    try(PrintWriter expected = new PrintWriter(
                            PATH + "E_n" + postfix + "_" + COLUMN + ".txt")) {
                        expected.println("E[Rp] = " + PDF.expected(frp));
                        expected.println("E[Rd] = " + PDF.expected(frd));
                        expected.println("E[Z] = " + PDF.expected(fz));
                        expected.println("E[Pd] = " + PDF.expected(Pd));
                        expected.println("E[R] = " +
                                (PDF.expected(frp) +
                                (PDF.expected(fz) + PDF.expected(frd))*PDF.expected(Pd)));
                    }
                }
            }
        }
    }
}
