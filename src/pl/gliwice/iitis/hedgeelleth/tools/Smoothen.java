/*
 * Smoothen.java
 *
 * Created on Mar 18, 2014
 *
 * Copyright (c) 2014  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.tools;

import java.util.*;
import java.io.*;

import pl.gliwice.iitis.hedgeelleth.compiler.util.text.NumStream;

/**
 * Smoothens y values.
 * 
 * @author Artur Rataj
 */
public class Smoothen {
    /**
     * Smoothens y values, by getting a mean value of three neighboring
     * points. Missing points are extrapolated by copying alues of their neighbours.
     * 
     * @param in input array
     * @param iterations iterations, can be zero
     * @return a new output array
     */
    public static double[][] process(double[][] in, int iterations) {
        double[][] out = new double[in.length][2];
        if(iterations == 0) {
            for(int i = 0; i < in.length; ++i) {
                out[i][0] = in[i][0];
                out[i][1] = in[i][1];
            }
        } else {
            for(int it = 0; it < iterations; ++it) {
                for(int i = 0; i < in.length; ++i) {
                    double sum = 0.0;
                    for(int x = -1; x<= 1; ++x)
                        sum += in[Math.max(0, Math.min(in.length - 1, i + x))][1];
                    out[i][0] = in[i][0];
                    out[i][1] = sum/3.0;
                }
                if(it + 1 < iterations) {
                    for(int i = 0; i < in.length; ++i)
                        in[i][1] = out[i][1];
                }
            }
        }
        return out;
    }
    public static void main(String[] args) throws IOException {
        String[] args_ = {
            "/home/art/projects/lte/pdf-close.txt",
            "/home/art/projects/lte/m.txt",
            "2",
        };
        args = args_;
        if(args.length != 0) {
            System.out.println("arguments: <input file> <output file> <smoothing iterations>");
            System.out.println("Each iteration computes an average of three subsequent elements, if any.");
        }
        int iterations = Integer.parseInt(args[2]);
        double[][] in = NumStream.toArray(new FileInputStream(args[0]));
        if(in[0].length != 2)
            throw new IOException("a pair of numbers per line expected");
        double[][] out = process(in, iterations);
        NumStream.fromArray(out, new FileOutputStream(args[1]));
    }
}
