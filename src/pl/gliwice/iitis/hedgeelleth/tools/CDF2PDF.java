/*
 * CDF2PDF.java
 *
 * Created on Mar 18, 2014
 *
 * Copyright (c) 2014  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.tools;

import java.util.*;
import java.io.*;

import pl.gliwice.iitis.hedgeelleth.compiler.util.text.NumStream;

/**
 * Converts a discrete CDF to a PDF.
 * 
 * @author Artur Rataj
 */
public class CDF2PDF {
    public static void main(String[] args) throws IOException {
        String[] args_ = {
            "/home/art/projects/lte/fig3-duration.csv",
            "/home/art/projects/lte/pdf-duration.txt",
            "4", "3",
        };
        // args = args_;
        if(args.length < 2 || args.length > 4) {
            System.out.println("arguments: <input file> <output file> [<distance between compared points>] [<smoothing iterations>]");
            System.out.println("The defaults are distance is 2 and no smoothing.");
        }
        int DISTANCE;
        if(args.length >= 3)
            DISTANCE = Integer.parseInt(args[2]);
        else
            DISTANCE = 2;
        int SMOOTHING;
        if(args.length >= 4)
            SMOOTHING = Integer.parseInt(args[3]);
        else
            SMOOTHING = 0;
        double[][] cdf = NumStream.toArray(new FileInputStream(args[0]));
        if(cdf[0].length != 2)
            throw new IOException("a pair of numbers per line expected");
        double[][] pdf = new double[cdf.length - DISTANCE][2];
        for(int i = DISTANCE; i < cdf.length; ++i) {
            double diff = cdf[i][1] - cdf[i - DISTANCE][1];
            if(false)
                // use average x values
                pdf[i - DISTANCE][0] = (cdf[i - DISTANCE][0] + cdf[i][0])/2.0;
            else {
                // stretch x values to whole CDF domain
                double ratio = (i - DISTANCE)*1.0/(cdf.length - 2);
                pdf[i - DISTANCE][0] = ((1.0 - ratio)*cdf[i - DISTANCE][0] + ratio*cdf[i][0]);
            }
            pdf[i - DISTANCE][1] = diff;
        }
        double[][] smooth = Smoothen.process(pdf, SMOOTHING);
        boolean NEGATIVE = false;
        for(int i = 0; i < smooth.length; ++i)
            if(smooth[i][1] < 0) {
                smooth[i][1] = 0.0;
                NEGATIVE = true;
            }
        if(NEGATIVE)
            System.out.println("WARNING: negative density, corrected");
        NumStream.fromArray(smooth, new FileOutputStream(args[1]));
    }
}
