/*
 * PStates.java
 *
 * Created on Mar 26, 2013
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.tools;

import java.util.*;
import java.io.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.CompilerUtils;

/**
 * Loads a Prism's <code>*.states</code> file.
 * 
 * @author Artur Rataj
 */
public class PStates {
    /**
     * Number of state variables.
     */
    public int varNum;
    /**
     * Number of states.
     */
    public int numStates;
    /**
     * Names of subsequent state variables.
     */
    public List<String> names;
    /**
     * Subsequent states.
     */
    public List<int[]> states;
    /**
     * Initial state, -1 for unknown.
     */
    public int initial;
    /**
     * Creates an empty list of states, no initial state known.
     * 
     * @param names names of subsequent state variables
     */
    public PStates(List<String> names) {
        this.names = names;
        varNum = this.names.size();
        states = new ArrayList<>();
        numStates = 0;
        initial = -1;
    }
    /**
     * Creates a new set of states from a file.
     * 
     * @param statesFilename name of the file with states
     * @param statesFilename name of the file with labels; null for an unknown initial state
     */
    public PStates(String statesFilename, String labelsFilename) throws IOException {
        names = new ArrayList<>();
        states = new ArrayList<>();
        InputStream in = new FileInputStream(statesFilename);
        Scanner scLine = CompilerUtils.newScanner(in);
        boolean namesLine = true;
        long lineCount = 0;
        System.out.println("loading states...");
        while(scLine.hasNextLine()) {
            ++lineCount;
            final int LINE_M = 1000000;
            if(lineCount%(LINE_M + 1) == LINE_M)
                System.out.println(lineCount/LINE_M + "m");
            String line = scLine.nextLine();
            if(namesLine) {
                if(line.charAt(0) != '(' || line.charAt(line.length() - 1) != ')')
                    throw new IOException(statesFilename + ": " + lineCount + ": invalid parentheses");
                line = line.substring(1, line.length() - 1);
                Scanner scNames = CompilerUtils.newScanner(line).useDelimiter(",");
                int count = 0;
                while(scNames.hasNext()) {
                    String s = scNames.next();
                    names.add(s);
                }
                varNum = names.size();
                if(varNum == 0)
                    throw new IOException(statesFilename + ": " + lineCount + ": missing names of variables");
                namesLine = false;
            } else {
                if(line.isEmpty())
                    break;
                int pos = line.indexOf(":");
                if(pos == -1)
                    throw new IOException(statesFilename + ": " + lineCount + ": missing `:'");
                long index;
                try {
                    index = Long.parseLong(line.substring(0, pos));
                } catch(NumberFormatException e) {
                    throw new IOException(statesFilename + ": " + lineCount +
                            ": invalid state index: " + e.toString());
                }
                if(index != lineCount - 2)
                    throw new IOException(statesFilename + ": " + lineCount +
                            ": state index out of order, expected " + (lineCount - 2));
                if(line.length() < pos + 3)
                    throw new IOException(statesFilename + ": " + lineCount +
                            ": line too short");
                if(line.charAt(pos + 1) != '(' || line.charAt(line.length() - 1) != ')')
                    throw new IOException(statesFilename + ": " + lineCount + ": invalid parentheses");
                line = line.substring(pos + 2, line.length() - 1);
                int[] s = new int[varNum];
//                Scanner scNumbers = new Scanner(line).useDelimiter(",").useLocale(Locale.ROOT);
//                int count = 0;
//                while(scNumbers.hasNextInt()) {
//                    int i = scNumbers.nextInt();
//                    if(count == varNum)
//                        throw new IOException(statesFilename + ": " + lineCount + ": too many values");
//                    s[count++] = i;
//                }
                StreamTokenizer stNumbers = new StreamTokenizer(new StringReader(line));
                stNumbers.whitespaceChars(',', ',');
                int count = 0;
                int ttype;
                while((ttype = stNumbers.nextToken()) != StreamTokenizer.TT_EOL &&
                        ttype != StreamTokenizer.TT_EOF) {
                    int i = (int)stNumbers.nval;
                    if(count == varNum)
                        throw new IOException(statesFilename + ": " + lineCount + ": too many values");
                    s[count++] = i;
                }
                if(count < varNum)
                    throw new IOException(statesFilename + ": " + lineCount + ": not enough values");
                states.add(s);
            }
        }
        System.out.println("loaded " + states.size() + " states, ok.");
        numStates = states.size();
        if(labelsFilename != null)
            initial = getInitialState(labelsFilename);
        else
            initial = -1;
    }
    /**
     * Finds the initial state.
     * 
     * @param labelsFilename name of the labels file
     * @return initial state
     */
    private int getInitialState(String labelsFilename) throws IOException {
        int init = -1;
        String labels = CompilerUtils.readFile(new File(labelsFilename));
        int initClassId = -1;
        Scanner sc = CompilerUtils.newScanner(labels);
        String clazz = sc.nextLine();
        Scanner scClazz = CompilerUtils.newScanner(clazz);
        while(scClazz.hasNext()) {
            String c = scClazz.next();
            int pos = c.indexOf('=');
            if(pos == -1)
                throw new IOException("no separator found in label class description");
            int value;
            try {
                value = Integer.parseInt(c.substring(0, pos));
            } catch(NumberFormatException e) {
                throw new IOException("no label value found in label class description");
            }
            String name = c.substring(pos + 1);
            if(!name.startsWith("\"") || !name.endsWith("\""))
                throw new IOException("quoted label class name expected");
            name = name.substring(1, name.length() - 1);
            if(name.equals("init"))
                initClassId = value;
        }
        if(initClassId == -1)
            throw new IOException("init label class description not found");
        while(sc.hasNextLine()) {
            String line = sc.nextLine();
            int pos = line.indexOf(':');
            if(pos == -1)
                throw new IOException("no separator found in state description");
            int index;
            int id;
            try {
                index = Integer.parseInt(line.substring(0, pos));
                id = Integer.parseInt(line.substring(pos + 1).trim());
                if(id == initClassId) {
                    if(init != -1)
                        throw new IOException("multiple initial states not supported");
                    init = index;
                }
            } catch(NumberFormatException e) {
                throw new IOException("malformed state description: numerical values expected");
            }
        }
        if(init == -1)
            throw new IOException("initial state not found");
        return init;
    }
    /**
     * Returns the number of variables, defining s single state.
     * 
     * @return number of variables, &gt; 0
     */
    public int getVarNum() {
        return varNum;
    }
    /**
     * Returns the number of states.
     * 
     * @return number of states, &gt;= 0
     */
    public int getStatesNum() {
        return numStates;
    }
    /**
     * Returns the name of a given variable.
     * 
     * @param index index of the variable, 0 ... <code>getVarNum()</code> - 1
     * @return that variable's name
     */
    public String getVarName(int index) {
        return names.get(index);
    }
    /**
     * Returns a given state.
     * 
     * @param index index of the state to return, 0 ... <code>getStatesNum()</code> - 1
     * @return array with values of subsequent state variables
     */
    public int[] getState(int index) {
        return states.get(index);
    }
    /**
     * Prints a string representation of a state.
     * 
     * @param index state number
     * @param longForm false for a set of space--separated numbers, true
     * for a decorated version with variable names
     * @return a textual description
     */
    public String stateToString(int index, boolean longForm) {
        StringBuilder out = new StringBuilder();
        if(longForm)
            out.append("[");
        Iterator<String> namesI = names.iterator();
        boolean first = true;
        for(int v : getState(index)) {
            if(!first) {
                if(longForm) {
                    out.append(", ");
                } else
                    out.append("\t");
            }
            if(longForm)
                out.append(namesI.next() + "=");
            out.append(v);
            first = false;
        }
        if(longForm)
            out.append("]");
        return out.toString();
    }
    @Override
    public String toString() {
        StringBuilder out = new StringBuilder();
        out.append("(");
        for(int v = 0; v < getVarNum(); ++v) {
            if(v != 0)
                out.append(",");
            out.append(names.get(v));
        }
        out.append(")\n");
        for(int s = 0; s < getStatesNum(); ++s) {
            out.append(s);
            out.append(":(");
            out.append(stateToString(s, false));
            out.append(")\n");
        }
        return out.toString();
    }
    public static void main(String[] args) throws IOException {
        String[] args_ = {
            "/home/art/projects/spreflo/HedgeellethCompiler/example/simple/CoinFlip.states",
        };
        // args = args_;
        if(args.length != 1)
            System.out.println("syntax: <input Prism's states file>");
        else {
            PStates s = new PStates(args[0], null);
        }
    }
}
