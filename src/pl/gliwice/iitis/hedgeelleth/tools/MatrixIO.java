/*
 * MatrixIO.java
 *
 * Created on May 10, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.tools;

import java.util.*;
import java.io.*;

import pl.gliwice.iitis.hedgeelleth.compiler.util.text.NumStream;

/**
 * Generates a file from a matrix.
 * 
 * @author Artur Rataj
 */
public class MatrixIO {
    /**
     * Generates a text file, representing a matrix.
     * 
     * @param outStream output stream, is not closed by this method
     * @param matrix transition matrix, containing rates
     */
    public static void generateText(FileOutputStream outStream,
            double[][] matrix) throws IOException {
        int states = matrix.length;
        PrintWriter out = new PrintWriter(outStream);
        for(int row = 0; row < states; ++row) {
            StringBuilder s = new StringBuilder();
            for(int col = 0; col < states; ++col) {
                double rate = matrix[row][col];
                s.append(rate);
                if(col != states - 1)
                    s.append(" ");
                else
                    s.append("\n");
            }
            out.print(s.toString());
        }
        out.flush();
        if(out.checkError())
            throw new IOException("could not write output file");
    }
    /**
     * Generates a Prism's nm file, representing a matrix. If the last state is absorbing,
     * a respective label \"absorbing\" is generated
     * 
     * @param name name of the module to generate
     * @param outStream output stream, is not closed by this method
     * @param matrix transition matrix, containing rates
     * @param modelComment a single--line commenting the model, null for none
     * @param rowComment a list with comments of subsequent rows, null for none;
     * the comments should not have the "// " prefix
     */
    public static void generateNm(String name,
            FileOutputStream outStream, double[][] matrix,
            String modelComment, List<String> rowComments)
                throws IOException {
        int states = matrix.length;
        Iterator<String> commentsI;
        if(rowComments != null)
            commentsI = rowComments.iterator();
        else
            commentsI = null;
        PrintWriter out = new PrintWriter(outStream);
        if(modelComment != null)
            out.write("// " + modelComment + "\n\n");
        out.write("ctmc\n\n");

        out.write("module " + name + "\n");

        String step = "";
        for(int i = name.length() - 1; i >= 0; --i) {
            char c = name.charAt(i);
            if(Character.isDigit(c))
                step = c + step;
            else
                break;
        }
        step = "s" + step;
        out.write("  " + step + " : [0.." + (states - 1) + "] init 0;\n\n");

        boolean absorbing = true;
        boolean toSelf = false;
        for(int row = 0; row < states; ++row) {
            if(commentsI != null)
                out.write("  // " + commentsI.next() + "\n");
            out.write("  [] " + step + "=" + row + " -> ");
            boolean first = true;
            for(int col = 0; col < states; ++col) {
                double rate = matrix[row][col];
                if(rate != 0) {
                    if(col == row)
                        toSelf = true;
                    else {
                        if(!first)
                            out.write(" + ");
                        out.write(rate + " : (" + step + "' = " + col + ")");
                        first = false;
                    }
                    if(row == states - 1 && col != row)
                        absorbing = false;
                }
            }
            if(first)
                out.write("true");
            out.write(";\n");
        }
        out.write("endmodule\n");
        if(absorbing) {
            out.write("\n// probability mass: P=? [ !\"absorbing\" U[tMin, tMax] \"absorbing\" ]\n");
            out.write("label \"absorbing\" = " + step + "=" + (states - 1) + ";\n");
        }
        out.flush();
        if(out.checkError())
            throw new IOException("could not write output file");
    }
    public static void main(String[] args) throws IOException {
        String[] args_ = {
            "matrix.txt",
            "a.nm",
        };
        // args = args_;
        if(args.length != 2)
            System.out.println("syntax: <plain dense matrix file> <output nm file>\n" +
                    "if the last state is absorbing, a respective label \"absorbing\" is generated");
        else {
            String inFileName = args[0];
            String outFileName = args[1];
            FileInputStream in = new FileInputStream(inFileName);
            FileOutputStream out = new FileOutputStream(outFileName);
            double[][] matrix = NumStream.toArray(in);
            int width = matrix[0].length;
            int height = matrix.length;
            if(width != height)
                throw new RuntimeException("square matrix expected");
            int pos = outFileName.lastIndexOf('.');
            String name;
            if(pos == -1)
                name = outFileName;
            else
                name = outFileName.substring(0, pos);
            generateNm(name, out, matrix, null, null);
            out.close();
            in.close();
        }
    }
}
