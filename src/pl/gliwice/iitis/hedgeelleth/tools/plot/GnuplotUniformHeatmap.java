/*
 * InterpolateLines.java
 *
 * Created on Jan 10, 2018
 *
 * Copyright (c) 2018  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.tools.plot;

import java.util.*;
import java.io.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.text.NumStream;

/**
 * Takes a heatmap with a non-uniform column, tries uniform.
 * Bleh.
 * 
 * @author Artur Rataj
 */
public class GnuplotUniformHeatmap {
    public static void main(String[] args) throws IOException {
        if(args.length < 2)
            System.out.println("syntax: <file name> <column number since 1> " +
                    "<grid step>");
        else {
            int col = Integer.parseInt(args[1]) - 1;
            try(InputStream stream = new FileInputStream(args[0])) {
                double[][] data = NumStream.toArray(stream);
                double min = data[0][col];
                double max = data[data.length - 1][col];
            }
            
        }
    }
}
