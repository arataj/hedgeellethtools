/*
 * PDFTool.java
 *
 * Created on Feb 14, 2017
 *
 * Copyright (c) 2017  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.tools;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.Map.Entry;
import pl.gliwice.iitis.hedgeelleth.compiler.util.text.NumStream;

/**
 *
 * @author Artur Rataj
 */
public class PDF {
    public static double[][] mapToArray(SortedMap<Double, Double> sort) {
        int len = sort.size();
        double[][] out = new double[len][2];
        int count = 0;
        for(Entry<Double, Double> e : sort.entrySet()) {
            out[count][0] = e.getKey();
            out[count][1] = e.getValue();
            ++count;
        }
        return out;
    }
    public static SortedMap<Double, Double> arrayToMap(double[][] pdf) {
        SortedMap<Double, Double> sort = new TreeMap<>();
        for(int i = 0; i < pdf.length; ++i)
            sort.put(pdf[i][0], pdf[i][1]);
        return sort;
    }
    public static double[][] mult(double[][] pdf, double m) {
        double[][] out = new double[pdf.length][2];
        for(int i = 0; i < pdf.length; ++i) {
            out[i][0] = pdf[i][0];
            out[i][1] = pdf[i][1]*m;
        }
        return out;
    }
    public static double[][] mult(double m, double[][] pdf) {
        return mult(pdf, m);
    }
    public static double[][] mult(double[][] pdf1, double[][] pdf2) {
        SortedMap<Double, Double> sort = arrayToMap(pdf1);
        for(int i = 0; i < pdf2.length; ++i) {
            double a = pdf2[i][0];
            Double v = sort.get(a);
            if(v == null)
                v = 0.0;
            sort.put(a, v*pdf2[i][1]);
        }
        return mapToArray(sort);
//        double[][] out = new double[pdf1.length][2];
//        for(int i = 0; i < Math.max(pdf1.length, pdf2.length); ++i) {
//            if(i < pdf1.length)
//                out[i][0] = pdf1[i][0];
//            else
//                out[i][0] = pdf2[i][0];
//            if(i < pdf1.length && i < pdf2.length && pdf1[i][0] != pdf2[i][0])
//                throw new RuntimeException("value mismatch");
//            out[i][1] = (i < pdf1.length ? pdf1[i][1]:0)*
//                    (i < pdf2.length ? pdf2[i][1]:0);
//        }
//        return out;
    }
    // not a convolution, simple addition
    public static double[][] add(double[][] pdf1, double[][] pdf2) {
        SortedMap<Double, Double> sort = arrayToMap(pdf1);
        for(int i = 0; i < pdf2.length; ++i) {
            double a = pdf2[i][0];
            Double v = sort.get(a);
            if(v == null)
                v = 0.0;
            sort.put(a, v + pdf2[i][1]);
        }
        return mapToArray(sort);
//        int maxLen = Math.max(pdf1.length, pdf2.length);
//        double[][] out = new double[maxLen][2];
//        for(int i = 0; i < maxLen; ++i) {
//            if(i < pdf1.length)
//                out[i][0] = pdf1[i][0];
//            else
//                out[i][0] = pdf2[i][0];
//            if(i < pdf1.length && i < pdf2.length && pdf1[i][0] != pdf2[i][0])
//                throw new RuntimeException("value mismatch");
//            out[i][1] = (i < pdf1.length ? pdf1[i][1]:0) +
//                    (i < pdf2.length ? pdf2[i][1]:0);
//        }
//        return out;
    }
    public static double[][] add(double[][] pdf, double v) {
        double[][] out = new double[pdf.length][2];
        for(int i = 0; i < pdf.length; ++i) {
            out[i][0] = pdf[i][0];
            out[i][1] = pdf[i][1] + v;
        }
        return out;
    }
    public static double[][] add(double v, double[][] pdf) {
        return add(pdf, v);
    }
    public static double[][] sub(double v, double[][] pdf) {
        double[][] out = new double[pdf.length][2];
        for(int i = 0; i < pdf.length; ++i) {
            out[i][0] = pdf[i][0];
            out[i][1] = v - pdf[i][1];
        }
        return out;
    }
    public static double[][] pow(double[][] pdf, double v) {
        double[][] out = new double[pdf.length][2];
        for(int i = 0; i < pdf.length; ++i) {
            out[i][0] = pdf[i][0];
            out[i][1] = Math.pow(pdf[i][1], v);
        }
        return out;
    }
    public static double expected(double[][] pdf) {
        double e = 0.0;
        for(int i = 0; i < pdf.length; ++i)
            e += pdf[i][0]*pdf[i][1];
        return e;
    }
    public static double[][] cdf(double[][] pdf, boolean half) {
        double[][] cdf = new double[pdf.length][2];
        double sum = 0.0;
        double diff = 0;
        for(int i = 0; i < pdf.length; ++i) {
            sum += pdf[i][1];
            if(half) {
                if(i < pdf.length - 1)
                    diff = pdf[i + 1][0] - pdf[i][0];
                cdf[i][0] = pdf[i][0] + diff/2.0;
            } else
                cdf[i][0] = pdf[i][0];
            cdf[i][1] = sum;
        }
        return cdf;
    }
    // scales up
    public static double[][] trapezoid(double[][] pdf, int mult) {
        int len = pdf.length;
        int outLen = pdf.length*mult;
        double[][] out = new double[outLen][2];
        double mid = (mult - 1)/2.0;
        for(int i = 0; i < len; ++i) {
            for(int j = 0; j < mult; ++j) {
                double x = i + (j - mid)/mult;
                double y;
                if(x < i)
                    y = pdf[Math.max(0, i - 1)][1]*(i - x) + pdf[i][1]*(x - (i - 1));
                else
                    y = pdf[i][1]*((i + 1) - x) + pdf[Math.min(len - 1, i + 1)][1]*(x - i);
                out[i*mult + j][0] = x;
                out[i*mult + j][1] = y/mult;
            }
        }
        return out;
    }
    static long PRECISION = Math.round(1e8);
    // scales down
    public static double[][] bucket(double[][] pdf, int div) {
        int len = pdf.length;
        int outLen = (int)Math.round(pdf.length*1.0/div);
        if(Math.abs(pdf.length*1.0/div - outLen) > 1e-10)
            throw new RuntimeException("not equally divisible");
        double[][] out = new double[outLen][2];
        for(int i = 0; i < outLen; ++i) {
//if(i == 590)
//    i = i;
            double sumX = 0.0;
            double sumY = 0.0;
            for(int j = 0; j < div; ++j) {
                int k = i*div + j;
                sumX += pdf[k][0];
                sumY += pdf[k][1];
            }
            out[i][0] = Math.round((sumX/div)*PRECISION)*1.0/PRECISION;
            out[i][1] = sumY;
        }
        return out;
    }
    public static double[][] convolute(double[][] pdf1, double[][] pdf2, boolean intX) {
        SortedMap<Double, Double> sort = new TreeMap<>();
        for(int i = 0; i < pdf1.length; ++i) {
            for(int j = 0; j < pdf2.length; ++j) {
                double x = Math.round((pdf1[i][0] + pdf2[j][0])*PRECISION)*1.0/PRECISION;
                double y = pdf1[i][1]*pdf2[j][1];
                Double sum = sort.get(x);
                if(sum == null)
                    sum = 0.0;
                sum += y;
                sort.put(x, sum);
            }
        }
        int len = sort.size();
        double[][] out = new double[len][2];
        int count = 0;
        for(Entry<Double, Double> e : sort.entrySet()) {
            double x = e.getKey();
            if(intX && Math.abs(x - Math.round(x)) > 1e-6)
                throw new RuntimeException("imprecision");
            double y = e.getValue();
            if(intX)
                out[count][0] = (int)Math.round(x);
            else
                out[count][0] = x;
            out[count][1] = y;
            ++count;
        }
        return out;
    }
    // if for each i, pdf[i][0] == i; otherwise a runtime exception is thrown
    public static void checkOrdered(double[][] pdf) {
        for(int i = 0; i < pdf.length; ++i)
            if(pdf[i][0] != i)
                throw new RuntimeException("not ordered: " + pdf[i][0] + " <> " + i);
    }
    public static void print(String fileName, double[][] pdf) {
        try(PrintWriter out = new PrintWriter(fileName)) {
            for(int i = 0; i < pdf.length; ++i)
                out.println(pdf[i][0] + "\t" + pdf[i][1]);
        } catch(IOException e) {
            throw new RuntimeException("count not write to " + fileName + ": " +
                    e.getMessage());
        }
    }
    public static double getMass(double[][] pdf) {
        return cdf(pdf, false)[pdf.length - 1][1];
    }
    public static double[][] normalize(double[][] pdf) {
        return mult(1.0/getMass(pdf), pdf);
    }
    // if the pdf sums to 1 +- margin; otherwise a runtime exception is thrown
    public static boolean testMass(double[][] pdf, double margin) {
        double testSum = getMass(pdf);
        if(testSum < 1 - margin || testSum > 1 + margin) {
//            throw new RuntimeException("insufficient precision: sum = " + testSum);
            System.out.println("insufficient precision: sum = " + testSum);
            return false;
        } else
            return true;
    }
    // removes 0's
    public static double[][] sparse(double[][] pdf) {
        SortedMap<Double, Double> sort = new TreeMap<>();
        for(int i = 0; i < pdf.length; ++i)
            if(pdf[i][1] > 1e-12)
                sort.put(pdf[i][0], pdf[i][1]);
        return mapToArray(sort);
    }
    // select column num as probabilities, i.e.
    // output has two columns 0, num; normally num=1
    public static double[][] column(double[][] pdf, int num) {
        double[][] selected = new double[pdf.length][2];
        for(int i = 0; i < pdf.length; ++i) {
            selected[i][0] = pdf[i][0];
            selected[i][1] = pdf[i][num];
        }
        return selected;
    }
    public static void main(String[] args) throws IOException {
        final String PATH = "/home/art/modelling/jozefiok/";
        for(int COLUMN = 1; COLUMN <= 5; ++COLUMN) {
            double[][] Pn = NumStream.toArray(new FileInputStream(PATH + "pn.txt"));
            Pn = PDF.column(Pn, COLUMN);
            System.out.println("column = " + COLUMN + " mass = " + PDF.getMass(Pn));
        }
    }
}
