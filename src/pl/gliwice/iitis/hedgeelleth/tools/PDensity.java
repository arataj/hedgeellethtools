/*
 * PDensity.java
 *
 * Created on May 10, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.tools;

import java.io.*;
import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.util.text.NumStream;
import pl.gliwice.iitis.hedgeelleth.math.rng.*;

/**
 * Generates Prism's PCTL file for getting a density function.
 * 
 * @author Artur Rataj
 */
public class PDensity {
    /**
     * Generates Prism's PCTL file for getting a density function.
     * 
     * @param outStream output stream, is not closed by this method
     * @param min minimum value
     * @param max maximum value
     * @param step a step between two subsequent samples
     */
    protected static void generatePctl(OutputStream outStream, double min, double max, double step) {
        PrintWriter out = new PrintWriter(outStream);
        for(double t = min; t <= max - 1e-6; t += step) {
            out.write("P=? [ !\"absorbing\" U[" + t + ", " + (t + step) + "] \"absorbing\" ]\n");
        }
        out.flush();
        if(out.checkError())
            throw new RuntimeException("could not write output file");
    }
    public static void main(String[] args) throws IOException {
        String[] args_ = {
            "0.0", "50.0", "1.0",
            "density.pctl",
        };
        // args = args_;
        if(args.length != 4)
            System.out.println("syntax: <min> <max> <step> <output pctl file>");
        else {
            double min = Double.parseDouble(args[0]);
            double max = Double.parseDouble(args[1]);
            double step = Double.parseDouble(args[2]);
            String outFileName = args[3];
            FileOutputStream out = new FileOutputStream(outFileName);
            generatePctl(out, min, max, step);
            out.close();
        }
    }
}
