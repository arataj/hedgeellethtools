/*
 * PSelectDistribution.java
 *
 * Created on Apr 5, 2013
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.tools;

import java.util.*;
import java.io.*;

/**
 * Prints a distribution of a selected set of states.
 * 
 * @author Artur Rataj
 */
public class PSelectDistribution {
    /**
     * Defines values of a single state variable. If it is a range of
     * values, then the respective variable's probability distribution against
     * values in that range is returned.
     */
    public static class VariableValue {
        /**
         * Minimum value.
         */
        public int min;
        /**
         * Maximum value.
         */
        public int max;
        /**
         * <p>True if a fixed value, false if a range of values.</p>
         * 
         * <p>If true, then <code>min == max</code>. If false, then the equality
         * may still hold.</p>
         */
        public boolean fixed;
        /**
         * A fixed value.
         * 
         * @param value a fixed value
         */
        public VariableValue(int value) {
            this.min = value;
            this.max = value;
            fixed = true;
        }
        /**
         * A range of values.
         * 
         * @param min minimum value
         * @param max maximum value
         */
        public VariableValue(int min, int max) {
            this.min = min;
            this.max = max;
            fixed = false;
        }
    };
    /**
     * A probability vector.
     */
    final PProbabilityVector PV;
    /**
     * Number of state variables.
     */
    final int VAR_NUM;
    /**
     * Number of states.
     */
    final int NUM;
    /**
     * Creates a new instance of <code>PSelectDistribution</code>.
     * 
     * @param pv a probability vector, from which a set of states is to be selected
     */
    public PSelectDistribution(PProbabilityVector pv) {
        PV = pv;
        VAR_NUM = PV.states.varNum;
        NUM = PV.probability.length;
    }
    /**
     * Returns a distribution of a variable, for which a range is
     * defined.
     * 
     * @param values values of successive variables
     * @return an array of probabilities, covering successive values in the range,
     * or null, if not a single matching state found
     */
    public double[] getDistribution(List<VariableValue> values) {
        double[] distribution = null;
        if(values.size() != VAR_NUM)
            throw new RuntimeException("expected " + VAR_NUM + " definitions " +
                    "of state variable values, found " + values.size());
        int MIN = 0;
        int MAX = 0;
        for(int index = 0; index < NUM; ++index) {
            int[] state = PV.states.getState(index);
            boolean matches = true;
            int coordinate = 0;
            boolean coordinateFound = false;
            int v = 0;
            for(VariableValue value : values) {
                if(value.fixed) {
                    if(state[v] != value.min)
                        matches = false;
                } else {
                    if(coordinateFound)
                        throw new RuntimeException("only a single variable's distribution " +
                                "can be selected");
                    coordinate = state[v];
                    if(coordinate < value.min || coordinate > value.max)
                        matches = false;
                    else {
                        coordinateFound = true;
                        if(distribution == null) {
                            MIN= value.min;
                            MAX = value.max;
                            distribution = new double[MAX - MIN + 1];
                        }
                    }
                }
                if(!matches)
                    break;
                ++v;
            }
            if(matches) {
                if(!coordinateFound)
                    throw new RuntimeException("no variable distribution selected");
                double p = PV.probability[index];
                distribution[coordinate - MIN] = p;
            }
        }
        return distribution;
    }
    public static void main(String[] args) throws IOException {
        String[] args_ = {
            "/home/art/projects/spreflo/HedgeellethCompiler/example/simple/100.states",
            "/home/art/projects/spreflo/HedgeellethCompiler/example/simple/100.pv",
            "100", "30~70", "0",
        };
        // args = args_;
        if(args.length <= 2)
            System.out.println("syntax: <input Prism's states file> <input Prism's probability vector  file> " +
                    "<fixed value of 1st variable> | ( <min value of 1st variable>~<max value of 1st variable> ) ...");
        else {
            PStates s = new PStates(args[0], null);
            PProbabilityVector a = new PProbabilityVector(s, args[1]);
            List<VariableValue> values = new ArrayList<>();
            boolean rangedFound = false;
            int min = 0;
            int max = 0;
            for(int i = 2; i < args.length; ++i) {
                VariableValue value;
                String t = args[i];
                int pos = t.indexOf("~");
                if(pos != -1) {
                    if(rangedFound)
                        throw new IOException("only a single variable can have a range of values");
                    if(pos == 0)
                        throw new IOException("minimum value missing");
                    if(pos == t.length() - 1)
                        throw new IOException("maximum value missing");
                    String minStr = t.substring(0, pos);
                    String maxStr = t.substring(pos + 1);
                    try {
                        min = Integer.parseInt(minStr);
                    } catch(NumberFormatException e) {
                        throw new IOException("invalid minimum value `" + minStr + "'");
                    }
                    try {
                        max = Integer.parseInt(maxStr);
                    } catch(NumberFormatException e) {
                        throw new IOException("invalid maximum value `" + maxStr + "'");
                    }
                    value = new VariableValue(min, max);
                    rangedFound = true;
                } else {
                    int fixed;
                    try {
                        fixed = Integer.parseInt(t);
                    } catch(NumberFormatException e) {
                        throw new IOException("invalid fixed value `" + t + "'");
                    }
                    value = new VariableValue(fixed);
                }
                values.add(value);
            }
            if(!rangedFound)
                throw new IOException("no variable has a range of values");
            PSelectDistribution sd = new PSelectDistribution(a);
            double[] probabilities = sd.getDistribution(values);
            for(int i = 0; i < probabilities.length; ++i)
                System.out.println((min + i) + " " + probabilities[i]);
        }
    }
}
