/*
 * DrawAdversarySoda.java
 *
 * Created on Mar 26, 2013
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.tools.test;

import java.awt.image.BufferedImage;
import java.util.*;
import java.io.*;
import javax.imageio.ImageIO;

import pl.gliwice.iitis.hedgeelleth.tools.*;

/**
 * Draws choices in a coin flip adversary generated from
 * <code>HedgeellethCompiler/example/simple/CoinFlip.java</code>.
 * 
 * @author Artur Rataj
 */
public class DrawAdversarySoda {
    enum Decision {
        MISSING,
        UP,
        DOWN,
        STABLE,
    }
    static int[] BLACK = {0, 0, 0};
    static int[] GRAY = {192, 192, 192};
    static int[] DARK_GRAY = {64, 64, 64};
    static int[] WHITE = {255, 255, 255};
    static int[] ORANGE = {255, 128, 0};
    static int[] GREEN = {32, 192, 32};
    static int[] RED = {255, 64, 64};
    static int[] BLUE = {0, 128, 255};
    static int[] YELLOW = {140, 140, 32};
    private static void plot(BufferedImage image, int day, int price, Decision decision,
            int variation) {
        int[] color;
        switch(decision) {
            case MISSING:
                color = DARK_GRAY;
                break;
                
            case UP:
                color = GREEN;
                break;
                
            case DOWN:
                color = RED;
                break;
                
            case STABLE:
                color = YELLOW;
                break;
                
            default:
                throw new RuntimeException("unknown decision");
        }
        if(variation != 0) {
            color = Arrays.copyOf(color, color.length);
            if(variation == 90)
                variation = variation;
            if(color[1] < 100) {
                color[1] += variation*50;
                color[2] += variation*50;
            } else {
                color[1] += variation*20;
                color[2] += variation*20;
            }
            color[2] = color[1] = variation;
            color[0] = (color[0] + variation)/2;
        }
//        if(flips == 93 && image.getHeight() - 1 - heads == 998)
//            flips = flips;
        image.getRaster().setPixel(day, image.getHeight() - 1 - price, color);
    }
    private static int toComponent(double real) {
        return (int)Math.round(real*0xff);
    }
    private static void plotRainbow(BufferedImage image, int x, int y, double color) {
        double UL = 0.1;
        double LOW = 0.2;
        double HIGH = 0.8;
        double UH = 0.9;
        double MARGIN = (HIGH - LOW)/3;
        double r, g, b;
        // color = y*1.0/image.getHeight();
        if(color < UL) {
            r = 0;
            g = 0.5 + 0.5*color/UL;
            b = 1.0 - color/UL;
        } else if(color < LOW) {
            double ratio = (color - UL)/(LOW - UL);
            r = ratio*0.5;
            g = 1.0;
            b = 0;
        } else if(color < HIGH) {
            double ratio = (color - LOW)/(HIGH - LOW);
            r = 0.5 + ratio*0.5;
            g = 1.0 - ratio*0.5;
            b = 0;
            if(color > LOW + MARGIN && color < HIGH - MARGIN) {
                double brighten = 1.0 -
                        Math.abs((color - (LOW + HIGH)/2)/(LOW + MARGIN - (LOW + HIGH)/2));
                r = (1.0 - brighten)*r + brighten;
                g = (1.0 - brighten)*g + brighten;
                b = (1.0 - brighten)*b + brighten;
            }
        } else if(color < UH) {
            double ratio = (color - HIGH)/(UH - HIGH);
            r = 1.0;
            g = 0.5 - ratio*0.5;
            b = 0;
        } else {
            double ratio = (color - UH)/(1.0 - UH);
            r = 1.0;
            g = 0;
            b = ratio;
        }
        int[] rgb = {toComponent(r)/8, toComponent(g)/8, toComponent(b)/8};
        int yFlipped = image.getHeight() - 1 - y;
//        if(x < 0 || x >= image.getWidth() || y < 0 || y >= image.getHeight())
//            x = x;
        int[] background = new int[3];
        image.getRaster().getPixel(x, yFlipped, background);
        for(int i = 0; i < rgb.length; ++i)
            rgb[i] = Math.min(0xff, rgb[i] + background[i]);
        image.getRaster().setPixel(x, yFlipped, rgb);
    }
    /**
     * Draws an image, that visualizes optimal decisions in the choose a coin
     * problem.
     * 
     * @param flipsNum total number of flips
     * @param adv adversary to visualize
     * @return raster image containing the visualization, of the size
     * <code>flipsNum</code> by <code>flipsNum</code>
     */
    private static BufferedImage draw_price(int days, int units, PAdversary adv) throws IOException {
        PrintWriter out = new PrintWriter("/tmp/space.txt");
        int SCALE = 20;
        int WD = days - 1;
        int HU =  units + 1;
        int W = WD*HU;
        int H = HU*HU;
        BufferedImage image = new BufferedImage(W + SCALE, H,
                BufferedImage.TYPE_3BYTE_BGR);
        for(int x = 0; x < W; ++x)
            for(int y = 0; y < H; ++y)
                if((x/WD%2 + y/HU%2)%2 == 1)
                    plot(image, x, y, Decision.MISSING, 0);
        for(int c = 0; c < 5; ++c)
            for(int x = W + 4; x < W + SCALE; ++x)
                for(int y = 0; y < H; ++y)
                    plotRainbow(image, x, y, y*1.0/H);
        for(long source : adv.getSourceStates()) {
            int[] sourceState = adv.getStates().getState((int)source);
            int s0 = sourceState[0];
            int n = sourceState[1];
            int c1 = sourceState[2];
            int c2 = sourceState[3];
            int p1 = sourceState[4];
            int p2 = sourceState[5];
            if(s0 == 4) {
                //int knowledge = sourceState[3];
                List<PAdversary.Transition> xList = adv.getTransitions(source);
                if(xList.size() != 1)
                    throw new RuntimeException("expected single transition");
                Decision d;
                int[] targetState = adv.getStates().getState((int)xList.get(0).targetIndex);
                int c1Next = targetState[2];
                if(n == days - 1)
                    n = n;
                if(c1Next > c1)
                    d = Decision.UP;
                else if(c1Next < c1)
                    d = Decision.DOWN;
                else
                    d = Decision.STABLE;
                //plot(image, (n - 1) + WD*p1 + WD*HU*p2, , d, 0/*knowledge*20*//*2 - Math.abs(knowledge - 90)*/);
                plotRainbow(image, (n - 1) + WD*p1, c2 + HU*p2, c1*1.0/units);
                out.println(n + "\t" + c1 + "\t" + c2 + "\t" + p1 + "\t" + p2);
            }
        }
        out.close();
        return image;
    }
    public static void main(String[] args) throws IOException {
        String[] args_ = {
            "20", "20",
            "/home/art/projects/svn/HedgeellethOptimiser/example/soda/Soda.sta",
            "/home/art/projects/svn/HedgeellethOptimiser/example/soda/Soda.tra",
            "/home/art/projects/svn/HedgeellethOptimiser/example/soda/Soda.adv",
            "/tmp/adv.png"
        };
        args = args_;
        if(args.length != 6)
            System.out.println("syntax: <number of flips> <input Prism's states file> " +
                    "<input Prism's adversary file> <file to save the output image>");
        else {
            int daysNum = Integer.parseInt(args[0]);
            int maxPrice = Integer.parseInt(args[1]);
            PStates states = new PStates(args[2], null);
            PAdversary a = new PAdversary(states, args[3],
                    new PStrategy(states, args[4]));
            System.out.println("saving the strategy...");
            a.saveTra("/tmp/filtered.adv");
            // BufferedImage out = draw_CoinFlip(flipsNum, a);
            System.out.println("drawing the strategy image...");
            BufferedImage out = draw_price(daysNum, maxPrice, a);
            ImageIO.write(out, "PNG", new File(args[5]));
        }
    }
}
