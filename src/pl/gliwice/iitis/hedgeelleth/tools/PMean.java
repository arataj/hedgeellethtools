/*
 * PMean.java
 *
 * Created on May 11, 2017
 *
 * Copyright (c) 2017  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.tools;

import java.io.*;

import pl.gliwice.iitis.hedgeelleth.compiler.util.text.NumStream;

/**
 * Computes mean value of a PDF.
 * 
 * @author Artur Rataj
 */
public class PMean {
    public static void main(String[] args) throws IOException {
        if(args.length != 1)
            System.out.println("syntax: <pdf, column 0 = value, column 1 = probability>");
        else {
            FileInputStream in = new FileInputStream(args[0]);
            double[][] pdf = NumStream.toArray(in);
            double mean = 0.0;
            for(double[] row : pdf) {
                mean += row[0]*row[1];
            } 
            in.close();
            System.out.println("mean = " + mean);
        }
    }
}
