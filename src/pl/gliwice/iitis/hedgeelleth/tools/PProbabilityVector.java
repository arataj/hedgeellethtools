/*
 * PProbabilityVector.java
 *
 * Created on Mar 26, 2013
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.tools;

import java.util.*;
import java.io.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.CompilerUtils;

/**
 * Loads a Prism's probability vector, from files <code>*.states</code> and
 * <code>*.pv</code>.
 * 
 * @author Artur Rataj
 */
public class PProbabilityVector {
    /**
     * Number of states.
     */
    long statesNum;
    /**
     * States, over which is defined this adversary.
     */
    PStates states;
    /**
     * Probability of each state. Keyed with a state index.
     */
    double[] probability;
    /**
     * Creates a new probability vector.
     * 
     * @param states set of states
     * @param pvFilename name of the file with a probability vector
     */
    public PProbabilityVector(PStates states, String pvFilename) throws IOException {
        this.states = states;
        probability = new double[this.states.states.size()];
        InputStream in = new FileInputStream(pvFilename);
        Scanner scLine = CompilerUtils.newScanner(in);
        long lineCount = 0;
        while(scLine.hasNextLine()) {
            ++lineCount;
            String line = scLine.nextLine();
            int pos = line.indexOf('=');
            if(pos == -1)
                throw new IOException(pvFilename + ": " + lineCount + ": missing `='");
            if(pos == line.length() - 1)
                throw new IOException(pvFilename + ": " + lineCount + ": missing probability");
            String indexStr = line.substring(0, pos);
            if(indexStr.isEmpty())
                throw new IOException(pvFilename + ": " + lineCount + ": missing state index");
            long index;
            try {
                index = Long.parseLong(indexStr);
            } catch(NumberFormatException e) {
                throw new IOException(pvFilename + ": " + lineCount +
                        ": invalid state index: " + e.toString());
            }
            String probabilityStr = line.substring(pos + 1);
            if(probabilityStr.isEmpty())
                throw new IOException(pvFilename + ": " + lineCount + ": missing probability");
            double p;
            try {
                p = Double.parseDouble(probabilityStr);
            } catch(NumberFormatException e) {
                throw new IOException(pvFilename + ": " + lineCount +
                        ": invalid probability: " + e.toString());
            }
            if(index != lineCount - 1)
                throw new IOException(pvFilename + ": " + lineCount +
                        ": unordered state");
            if(index >= probability.length)
                throw new IOException(pvFilename + ": " + lineCount +
                        ": state index too large");
            probability[(int)index] = p;
        }
        if(lineCount != probability.length)
            throw new IOException(pvFilename + ": " + lineCount +
                    ": missing state probability");
    }
    /**
     * Returns the set of states, over which this probability vector is defined.
     * 
     * @return a set of states
     */
    public PStates getStates() {
        return states;
    }
    /**
     * Returns the probability of a state of a given index.
     * 
     * @param index state index
     * @return probability
     */
    public double getProbability(long index) {
        if(index > probability.length)
            throw new RuntimeException("state index too large");
        return probability[(int)index];
    }
    public static void main(String[] args) throws IOException {
        String[] args_ = {
            "/home/art/projects/spreflo/HedgeellethCompiler/example/simple/400.states",
            "/home/art/projects/spreflo/HedgeellethCompiler/example/simple/400.pv",
        };
        // args = args_;
        if(args.length != 2)
            System.out.println("syntax: <input Prism's states file> <input Prism's probability vector  file>");
        else {
            PStates s = new PStates(args[0], null);
            PProbabilityVector a = new PProbabilityVector(s, args[1]);
        }
    }
}
