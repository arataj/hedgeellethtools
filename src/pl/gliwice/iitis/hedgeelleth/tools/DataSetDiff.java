/*
 * DataSetDiff.java
 *
 * Created on Jun 24, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.tools;

import java.util.*;
import java.io.*;

import pl.gliwice.iitis.hedgeelleth.compiler.util.text.NumStream;
import pl.gliwice.iitis.hedgeelleth.math.rng.ArrayDist;

/**
 *
 * @author Artur Rataj
 */
public class DataSetDiff {
    public static void main(String[] args) throws IOException {
        String[] args_ = {
            "hypererlang/tcp_size.txt",
            "hypererlang/density-300.cdf",
            "hypererlang/tcp_size_diff.txt",
            "300",
        };
        args = args_;
        ArrayDist dist1 =  new ArrayDist(
                ArrayDist.copyMass(NumStream.toArray(new FileInputStream(args[0])), 0, 0,
                    false, null), false);
        ArrayDist dist2 =  new ArrayDist(
                ArrayDist.copyMass(NumStream.toArray(new FileInputStream(args[1])), 0, 0,
                    false, null), false);
        double min = dist1.getDomainMin();
        double max = dist1.getDomainMax();
        int steps = Integer.parseInt(args[3]);
        PrintWriter out = new PrintWriter(args[2]);
        for(int s = 0; s < steps; ++s) {
            double v = min + s*(max - min)/(steps - 1);
            try {
                double d = dist2.getDensity(v) - dist1.getDensity(v);
                //double d = Math.abs(dist2.getDensity(v) - dist1.getDensity(v));
                //double d = dist2.getDensity(v/1140) - dist1.getDensity(v);
                out.println(v + "\t" + d);
            } catch(IllegalArgumentException e) {
                /* ignore */
            }
        }
        out.close();
    }
}
