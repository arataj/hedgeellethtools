/*
 * OEC2P.java
 *
 * Created on May 10, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.tools;

import java.io.*;

import pl.gliwice.iitis.hedgeelleth.compiler.util.text.NumStream;
import pl.gliwice.iitis.hedgeelleth.math.rng.ArrayDist;
import pl.gliwice.iitis.hedgeelleth.math.rng.OsogamiEC;

/**
 *    Osogami's moment matching [1], that approximates a tabularized function from a text
 *    file with a CTMC, outputted in Prism's language.
 * 
 *     [1] Osogami, Harchol-Balter, "Closed form solutions for mapping general distributions to
 *     quasi-minimal PH distributions", 2006.
 *
 * @author Artur Rataj
 */
public class OEC2P {
    /**
     * Generates a Prism's nm file, representing the Osogami's EC distribution,
     * that fits given data.
     * 
     * @param name name of the module to generate
     * @param outStream output stream, is not closed by this method
     * @param ec an Osogami's EC distribution
     */
    protected static void generateNm(String name, FileOutputStream outStream,
            OsogamiEC ec) {
        PrintWriter out = new PrintWriter(outStream);
        out.write("ctmc\n\n");

        out.write("const int n = " + ec.getN() + ";\n");
        out.write("const double p = " + ec.getP() + ";\n");
        out.write("const double lambda_y = " + ec.getLambdaY() + ";\n");
        out.write("const double px = " + ec.getPX() + ";\n");
        out.write("const double lambda_x1 = " + ec.getLambdaX1() + ";\n");
        out.write("const double lambda_x2 = " + ec.getLambdaX2() + ";\n\n");

        out.write("label \"absorbing\" = ph=n+1;\n\n");
        
        out.write("module " + name + "\n");
        out.write("  ph : [0..n + 1] init 0;\n\n");

        // ugly approximation due to the lack of partial input probabilities in Prism
        out.write("  [] ph=0 -> (1 - p)*10000 : (ph' = n + 1) + p*10000 : (ph' = ph + 1);\n");
        out.write("  [] ph>=1 & ph<=n-2 -> lambda_y : (ph' = ph + 1);\n");
        out.write("  [] ph=n-1 -> (1 - px)*lambda_x1 : (ph' = n + 1) + px*lambda_x1 : (ph' = ph + 1);\n");
        out.write("  [] ph=n -> lambda_x2 : (ph' = ph + 1);\n");
        out.write("  [] ph=n+1 -> true;\n");
        out.write("endmodule\n");
        out.flush();
        if(out.checkError())
            throw new RuntimeException("could not write output file");
    }
    public static void main(String[] args) throws IOException {
        String[] args_ = {
            "../../test/przerwa_1.txt",
            "0.0",
            "a.nm",
        };
        // args = args_;
        if(args.length != 3)
            System.out.println("syntax: <input text file> <x align > <output nm file>\n" +
                    "align examples: 0 -- x defines section's x min, 0.5 -- x defines section center");
        else {
            String inFileName = args[0];
            double align = Double.parseDouble(args[1]);
            String outFileName = args[2];
            FileInputStream in = new FileInputStream(inFileName);
            FileOutputStream out = new FileOutputStream(outFileName);
            double[][] histo = NumStream.toArray(in);
            double width = histo[1][0] - histo[0][0];
            ArrayDist a = new ArrayDist(histo, width*align, width*(1.0 - align));
            // for(int x = 0; x < 50; ++x)
            //    System.out.println(a.getDensity(x));
            OsogamiEC ec = new OsogamiEC(
                a.getRawMoment(1), a.getRawMoment(2), a.getRawMoment(3));
            int pos = outFileName.lastIndexOf('.');
            String name;
            if(pos == -1)
                name = outFileName;
            else
                name = outFileName.substring(0, pos);
            generateNm(name, out, ec);
            out.close();
            in.close();
        }
    }
}
