/*
 * MCInterpreter.java
 *
 * Created on May 25, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.tools;

import java.util.*;
import java.io.*;
import pl.gliwice.iitis.hedgeelleth.tools.PAdversary.Transition;

/**
 * An interpreter of an deterministic Markov chains, or of
 * an adversary of a MDP.
 * 
 * @author Artur Rataj
 */
public class MCInterpreter {
    /**
     * A deterministic Markov chain or an adversary.
     */
    public final PAdversary ADVERSARY;
    /**
     * Index of the current state, -1 for not set.
     */
    public int stateIndex;
    
    /**
     * Creates a new interpreter, sets it into an initial (0th) state.
     * 
     * @param adversary an adversary
     */
    public MCInterpreter(PAdversary adversary) {
        ADVERSARY = adversary;
        setState(-1);
//        initState();
    }
    /**
     * Sets this interpreter to a given state.
     * 
     * @param index index of the state to set
     */
    public final void setState(int index) {
        stateIndex = index;
    }
//    /**
//     * Resets this interpreter to the initial (0th) state.
//     */
//    public void initState() {
//        setState(0);
//    }
    /**
     * Performs a single step.
     */
    public void step() {
        List<PAdversary.Transition> x = ADVERSARY.getTransitions(stateIndex);
if(x == null)
    x = x;
        if(x == null || x.size() == 0)
            throw new RuntimeException("deadlock in state " + stateIndex + "=" +
                    ADVERSARY.states.stateToString(stateIndex, true));
        else if(x.size() != 1) {
            double sum = 0.0;
            for(Transition t : x)
                sum += t.probability;
            if(sum > 1.0 + 1e-6)
                throw new RuntimeException("non--determinism in state " + stateIndex + "=" +
                        ADVERSARY.states.stateToString(stateIndex, true));
            else {
                double choice = Math.random();
                double low = 0.0;
                for(Transition t : x) {
                    double high = low + t.probability;
                    if(choice >= low && choice < high) {
                        long l = t.targetIndex;
                        if(l > Integer.MAX_VALUE)
                            throw new RuntimeException("state index does not fit into integer");
                        stateIndex = (int)l;
                        break;
                    }
                    low = high;
                }
            }
        } else {
            long l = x.get(0).targetIndex;
            if(l > Integer.MAX_VALUE)
                throw new RuntimeException("state index does not fit into integer");
            stateIndex = (int)l;
        }
    }
    /**
     * Prints the path from the current state up to another or to a self--loop,
     * whichever comes first.
     * 
     * @param outFileName name of the output file, null for none
     * @param stopState index of the last state to print, -1 for none
     * @param firstVarCondition if not -1, prints only states which begin
     * with that value
     * @return r1 - r2
     */
    public int printPath(String outFileName, int stopState, int firstVarCondition) throws IOException {
        PrintWriter out;
        if(outFileName != null)
            out = new PrintWriter(outFileName);
        else
            out = null;
        boolean first = true;
        if(out != null) {
            for(String s : ADVERSARY.states.names) {
                if(first)
                    first = false;
                else
                    out.print("\t");
                out.print(s);
            }
            out.println("\tr1\tr2");
        }
        int prevStateIndex = -1;
        int r1 = 0;
        int r2 = 0;
        do {
//System.out.println(stateIndex + "\t" + ADVERSARY.states.stateToString(stateIndex, false));
            int statNum = ADVERSARY.states.getState(stateIndex)[0];
            if(out != null) {
                if(firstVarCondition == -1 || firstVarCondition == statNum) {
                    out.print(ADVERSARY.states.stateToString(stateIndex, false));
                    out.println("\t" + r1 + "\t" + r2);
                }
            }
            if(statNum == 1)
                r1 += ADVERSARY.states.getState(stateIndex)[2];
            if(statNum == 2)
                r2 += ADVERSARY.states.getState(stateIndex)[3];
            prevStateIndex = stateIndex;
            step();
        } while(stateIndex != stopState && stateIndex != prevStateIndex);
        if(out != null) {
            out.close();
            if(out.checkError())
                throw new IOException("error writing to " + outFileName);
        }
        return r1 - r2;
    }
    public static void main(String[] args) throws IOException {
        //String prefix = "/home/art/swistak/modelling/abs3/" + "W3_";
        String prefix = "/home/art/swistak/soda2/" +
                "soda2-36";
//        String prefix = "/home/art/swistak/soda/" +
//                "soda2";
        String[] args_ = {
            prefix + ".sta",
            prefix + ".label",
            prefix + ".tra",
            prefix + ".adv",
            prefix + ".path",
        };
        args = args_;
        PStates states = new PStates(args[0], args[1]);
        PStrategy strategy = new PStrategy(states, args[3]);
        PAdversary adversary = new PAdversary(states, args[2], strategy);
        MCInterpreter i = new MCInterpreter(adversary);
        for(int j = 0; j < 20; ++j) {
            i.setState(states.initial);
            i.printPath(args[4] + j, -1, 0);
        }
//        while(false) {
//            long diff = 0;
//            long count = 0;
//            for(long j = 0; true; ++j) {
//                i.setState(states.initial);
//                diff = diff + i.printPath(null, -1, 0);
//                ++count;
//                String s = count + "";
//                boolean dec = true;
//                for(int k = s.length() - 1; k > 0; --k)
//                    if(s.charAt(k) != '0') {
//                        dec = false;
//                        break;
//                    }
//                if(dec)
//                    System.out.println(count + "\t" + diff*1.0/count);
//            }
//        }
    }
}
