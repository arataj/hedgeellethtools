/*
 * AddX.java
 *
 * Created on May 18, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.tools;

import java.io.*;

import pl.gliwice.iitis.hedgeelleth.compiler.util.text.NumStream;

/**
 * Adds the leftmost coordinate to a text file. The coordinate's value
 * is linearly mapped.
 * 
 * @author Artur Rataj
 */
public class AddX {
    public static void main(String[] args) throws IOException {
        String[] args_ = {
            "../../test/przerwa_1.txt",
            "0.0",
            "a.nm",
        };
        // args = args_;
        if(args.length != 4)
            System.out.println("syntax: <input text file> <min> <max> <output text file>");
        else {
            String inFileName = args[0];
            double min = Double.parseDouble(args[1]);
            double max = Double.parseDouble(args[2]);
            String outFileName = args[3];
            FileInputStream in = new FileInputStream(inFileName);
            FileOutputStream out = new FileOutputStream(outFileName);
            BufferedOutputStream bufferedOut = new BufferedOutputStream(out);
            PrintWriter writer = new PrintWriter(bufferedOut);
            double[][] histo = NumStream.toArray(in);
            int num = histo.length;
            for(int row = 0; row < num; ++row) {
                double argument = min + row*(max - min)/(num - 1.0);
                double[] value = histo[row];
                writer.print(argument);
                for(int col = 0; col < value.length; ++col)
                    writer.print(" " + value[col]);
                writer.println();
            }
            writer.flush();
            if(writer.checkError())
                throw new IOException("error writing output file");
            bufferedOut.close();
            out.close();
            in.close();
        }
    }
}
