/*
 * DrawAdversaryCoinFlip.java
 *
 * Created on Mar 26, 2013
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.tools.test;

import java.awt.image.BufferedImage;
import java.util.*;
import java.io.*;
import javax.imageio.ImageIO;

import pl.gliwice.iitis.hedgeelleth.tools.*;

/**
 * Draws choices in a coin flip adversary generated from
 * <code>HedgeellethCompiler/example/simple/CoinFlip.java</code>.
 * 
 * @author Artur Rataj
 */
public class DrawAdversaryCoinFlip {
    enum Decision {
        MISSING,
        FIRST,
        SECOND,
    }
    static int[] BLACK = {0, 0, 0};
    static int[] GRAY = {192, 192, 192};
    static int[] WHITE = {255, 255, 255};
    static int[] ORANGE = {255, 128, 0};
    static int[] BLUE = {0, 128, 255};
    private static void plot(BufferedImage image, int flips, int heads, Decision decision,
            int variation) {
        int[] color;
        switch(decision) {
            case MISSING:
                color = WHITE;
                break;
                
            case FIRST:
                color = BLACK;
                break;
                
            case SECOND:
                color = GRAY;
                break;
                
            default:
                throw new RuntimeException("unknown decision");
        }
        if(variation != 0) {
            color = Arrays.copyOf(color, color.length);
            if(variation == 90)
                variation = variation;
            if(color[1] < 100) {
                color[1] += variation*50;
                color[2] += variation*50;
            } else {
                color[1] += variation*20;
                color[2] += variation*20;
            }
            color[2] = color[1] = variation;
            color[0] = (color[0] + variation)/2;
        }
        if(flips == 93 && image.getHeight() - 1 - heads == 998)
            flips = flips;
        image.getRaster().setPixel(flips, image.getHeight() - 1 - heads, color);
    }
    /**
     * Draws an image, that visualizes optimal decisions in the choose a coin
     * problem.
     * 
     * @param flipsNum total number of flips
     * @param adv adversary to visualize
     * @return raster image containing the visualization, of the size
     * <code>flipsNum</code> by <code>flipsNum</code>
     */
    private static BufferedImage draw_CoinFlip(int flipsNum, PAdversary adv) {
        BufferedImage image = new BufferedImage(flipsNum, flipsNum,
                BufferedImage.TYPE_3BYTE_BGR);
        for(int flips = 0; flips < flipsNum; ++flips)
            for(int heads = 0; heads < flipsNum; ++heads)
                plot(image, flips, heads, Decision.MISSING, 0);
        for(long source : adv.getSourceStates()) {
            int[] sourceState = adv.getStates().getState((int)source);
            if(sourceState[2] == 1) {
                int flips = sourceState[0];
                int heads = sourceState[1];
                List<PAdversary.Transition> xList = adv.getTransitions(source);
                if(xList.size() != 1)
                    throw new RuntimeException("unpected a single transition");
                long target = xList.get(0).targetIndex;
                int[] targetState = adv.getStates().getState((int)target);
                Decision d;
                if(targetState[2] == 2)
                    // first coin chosen
                    d = Decision.FIRST;
                else if(targetState[2] == 3)
                    // second coin chosen
                    d = Decision.SECOND;
                else
                    throw new RuntimeException("invalid target state");
                plot(image, flips, heads, d, 0);
            }
        }
        return image;
    }
    /**
     * Draws an image, that visualizes optimal decisions in the choose a coin
     * problem.
     * 
     * @param flipsNum total number of flips
     * @param adv adversary to visualize
     * @return raster image containing the visualization, of the size
     * <code>flipsNum</code> by <code>flipsNum</code>
     */
    private static BufferedImage draw_tosses(int flipsNum, PAdversary adv) {
        BufferedImage image = new BufferedImage(flipsNum, flipsNum,
                BufferedImage.TYPE_3BYTE_BGR);
        for(int flips = 0; flips < flipsNum; ++flips)
            for(int heads = 0; heads < flipsNum; ++heads)
                plot(image, flips, heads, Decision.MISSING, 0);
        for(long source : adv.getSourceStates()) {
            int[] sourceState = adv.getStates().getState((int)source);
            if(sourceState[2] == 1) {
                int flips = sourceState[0];
                int heads = sourceState[1];
                //int knowledge = sourceState[3];
                List<PAdversary.Transition> xList = adv.getTransitions(source);
                if(xList.size() != 1)
                    throw new RuntimeException("expected single transition");
                Decision d;
                int[] targetState = adv.getStates().getState((int)xList.get(0).targetIndex);
                if(targetState[2] == 2)
                    d = Decision.FIRST;
                else if(targetState[2] == 3)
                    d = Decision.SECOND;
                else
                    throw new RuntimeException("unknown state");
                plot(image, flips, heads, d, 0/*knowledge*20*//*2 - Math.abs(knowledge - 90)*/);
            }
        }
        return image;
    }
    public static void main(String[] args) throws IOException {
        String[] args_ = {
            "100",
            "/home/art/projects/svn/HedgeellethOptimiser/CoinFlip.sta",
            "/home/art/projects/svn/HedgeellethOptimiser/CoinFlip.adv",
            //"/home/art/prism/adv.states",
            //"/home/art/prism/adv.tra",
            "/tmp/adv.png"
        };
        args = args_;
        if(args.length != 4)
            System.out.println("syntax: <number of flips> <input Prism's states file> " +
                    "<input Prism's adversary file> <file to save the output image>");
        else {
            int flipsNum = Integer.parseInt(args[0]);
            PAdversary a = new PAdversary(new PStates(args[1], null), args[2], null);
            // BufferedImage out = draw_CoinFlip(flipsNum, a);
            BufferedImage out = draw_tosses(flipsNum, a);
            ImageIO.write(out, "PNG", new File(args[3]));
        }
    }
}
